"""
Django settings for gestion project.
"""
# -*- coding: utf-8 -*-
import os
import sys

import django

from gesmj import infos

sys.path.append('c:\\users\\belia\\appdata\\local\\packages\\pythonsoftwarefoundation.python.3.7_qbz5n2kfra8p0\\localcache\\local-packages\\python37\\site-packages')

"""
    Define local variable
"""
KEY, HOST, DB_NAME, USER, PASSWORD, PORT, CHARSET, DB_ENGINE, EMAIL = infos.getter_setting()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# This is new:


PROJECT_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.dirname(PROJECT_DIR)
APPS_DIR = os.path.realpath(os.path.join(ROOT_DIR, 'gesmj'))
sys.path.append(APPS_DIR)

# # SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = KEY
DEBUG = True
ALLOWED_HOSTS = ['gesmj.pythonanywhere.com','*']


# Comment it when you have to recreat db.
ROOT_URLCONF = 'gesmj.urls'

# APPLICATION DEFINITION
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'gesmjgenerator.apps.GesmjgeneratorConfig',
    'gestion.apps.GestionConfig',
    'templatetags.template_extras',
    'ckeditor',
    'widget_tweaks',
    'corsheaders',
    'django_email_verification',
    'sorl.thumbnail',
    'django_jsonfield_backport'
]


#WhiteNoise helps Django manage static files (images, scripts, etc) to load your website faster
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

FILE_UPLOAD_HANDLERS = [
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler'
   
]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),
            os.path.join(os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),'create'),
            os.path.join(os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),'display'),
            os.path.join(os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),'pannels'),
            os.path.join(os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),'mails'),
            os.path.join(os.path.join(BASE_DIR,'gesmjgenerator', 'templates/gesmjgenerator/'),'edit'),
            #BASE_DIR+'/gesmjgenerator/templates/gesmjgenerator/',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media'
            ],
            "libraries":{
                'template_extras': 'gesmj.templatetags.template_extras',
            }
        },
    },
]


DATABASES = {
     'default': {
         'ENGINE': DB_ENGINE,
         'NAME': DB_NAME,
         'USER': USER,
         'PASSWORD': PASSWORD,
         'HOST': HOST,
         'PORT': PORT,
         'default-character-set': CHARSET,
         'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_ALL_TABLES'",
         }
     }
 }


has_native_json_fiel = True

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Change to custom user model.
AUTH_USER_MODEL = 'gestion.User'

SESSION_COOKIE_SAMESITE = 'Strict'

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]

# CORS
CORS_ORIGIN_ALLOW_ALL = True
""" 
CORS_ORIGIN_WHITELIST = ("https://gesmj.pythonanywhere.com/",
                         "https://gesmj.pythonanywhere.com/*",
                         "http://127.0.0.1:8000",
                         "http://localhost:8080",
                         "http://localhost",
                         "https://localhost:8000",
                         "gesmj.pythonanywhere.com")
CORS_ALLOWED_ORIGINS = [
    "http://localhost:8080",
    "http://127.0.0.1:8000",
]
"""
CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]

"""
    LOGIN SYSTEM
"""
LOGIN_URL = 'gesmjgenerator:index_view'
LOGIN_REDIRECT_URL = 'gesmjgenerator:dashboard_view'
LOGOUT_REDIRECT_URL = 'gesmjgenerator:index_view'

"""
    SESSIONS
"""
SESSION_ENGINE = 'django.contrib.sessions.backends.db'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
#SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_SAMESITE = 'Lax'

"""
    COOKIES POLICIES
"""
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_SAMESITE = 'Lax'

"""
    NOSNIF : PERMIT TO WEBNAV TO GUESS WHAT KIND OF FILE YOU RUN.
"""
SECURE_CONTENT_TYPE_NOSNIFF = False

"""
    TIMEZONE
"""
LANGUAGE_CODE="fr"
TIME_ZONE = 'Europe/Paris'
USE_I18N = True  # Désactive le module de traduction
USE_L10N = True
USE_TZ = True


"""
    STATIC FILE
"""

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "gesmjgenerator/static"),
]
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'gestion' + STATIC_URL)

"""
    MEDIA FILES used to store user's files 
"""
MEDIA_URL = '/media/'
MEDIA_URL_COMPLEX = os.path.join("gestion" + MEDIA_URL)
MEDIA_ROOT = os.path.join(BASE_DIR, "gestion" + MEDIA_URL)

"""
    CKEDITOR CONFIG FILE 
"""
CKEDITOR_UPLOAD_PATH = STATIC_ROOT+'/ckeditor'
CKEDITOR_CONFIGS = {
    'ckeditor_config': {
        'fillEmptyBlocks': False,
        'basicEntities': False,
        'autoParagraph': False,
        'toolbar': 'Full',
        'isreadonly': True,
    },
    'default':{
        'fillEmptyBlocks': False,
        'basicEntities': False,
        'autoParagraph': False,
        'toolbar': 'Full',
        'isreadonly': True,
    },
    'note_config':{
        'customConfig': CKEDITOR_UPLOAD_PATH+'ckeditor/config_note.js'
    }
}


"""
    JSON FIXTURES
"""
FIXTURE_DIRS = [
    BASE_DIR+'gestion/fixtures/pargen.json',
]

"""
    X FRAME
"""
X_FRAME_OPTIONS = 'SAMEORIGIN'
    

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gesmj.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
    django.setup()


 
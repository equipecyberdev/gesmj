from django import template
from django.template import Library, Node, TemplateSyntaxError

register = template.Library()


@register.simple_tag
def substract(value1, value2):
    """
        New filter to substract 2 values.
    """
    try:
        return value1 - value2
    except ValueError:
        raise TemplateSyntaxError("Tag must use 2 values; Use : '{%  substract value1 value2 %}'")


@register.simple_tag
def add(value1, value2):
    """
        New filter to add 2 values
    """
    try:
        return value1 + value2
    except ValueError:
        raise TemplateSyntaxError("Tag must use 2 values; Use : '{%  add value1 value2 %}'")


@register.simple_tag
def add_with_separator(value1, value2):
    """
        New filter to add 2 values
    """
    try:
        return "{}/{}".format(value1, value2)
    except ValueError:
        raise TemplateSyntaxError("Tag must use 2 values; Use : '{%  value1|add_with_separator:value2 %}'")


@register.simple_tag
def percent(ceil_xp, actual_xp):
    """
        New filter to divide values and return % 100
    """
    try:
        if actual_xp == 0 or ceil_xp == 0:
            result = 0
        else:
            result = round((actual_xp / ceil_xp) * 100)
        return result
    except ValueError:
        raise TemplateSyntaxError("Tag must use 2 values; Use : '{%  percent ceil_xp actual_xp  %}'")


@register.simple_tag
def from_int_to_string(word, int_value):
    """
        New filter to return concat words has String
    """
    try:
        return word + str(int_value)
    except ValueError:
        raise TemplateSyntaxError("Tag must use 2 values; Use : '{%  from_int_to_string word int_value  %}'")


@register.simple_tag
def times(number):
    """
        New filter that return loop of "number" times.
    """
    try:
        return range(number)
    except ValueError:
        raise TemplateSyntaxError("Tag must use 1 value; Use : '{%  times number  %}'")




@register.simple_tag
def define_var(var=None):
    """
        New filter that create a variable.
    """
    try:
        return var
    except ValueError:
        raise TemplateSyntaxError("Tag must use 1 value; Use : '{%  times number  %}'")


register.filter('substract', substract)
register.filter('add', add)
register.filter('add_with_separator', add_with_separator)
register.filter('percent', percent)
register.filter('from_int_to_string', from_int_to_string)
register.filter('times', times)
register.filter('define_var', define_var)
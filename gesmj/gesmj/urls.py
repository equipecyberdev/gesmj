
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import include, path
from django_email_verification import urls as mail_urls

from gesmjgenerator import views as generator_views
from gestion import views as gestion_views

gestion_pattern = [
    # Element part
    path('update_element/<str:game_name>/<str:old_file_name>/<str:new_short_name>/<str:element_type>/<str:element_subtype>/<str:element_subtype_id>', gestion_views.update_element, name='update_element'),
    path('update_element/<str:game_name>/<str:old_file_name>/<str:new_short_name>/<str:element_type>/<str:id>/<str:old_thumbnail_name>', gestion_views.update_element, name='update_element_and_thumb'),
    path('delete_element/<str:game_name>/<str:document_name>/<int:id>/<str:element_type>/<str:element_subtype>', gestion_views.delete_element, name='delete_element'),    
    path('search_content', gestion_views.search_content_engine, name='search_content'),    

    # File part
    path('file_name_check/<str:game_name>/<str:type>/<str:target_name>/<str:id>/<str:order>', gestion_views.file_name_check, name='file_name_check'),
    path('dashboard/<str:game_name>/<str:file_type>/<str:id>/<str:color>', gestion_views.file_color_change, name='file_color_change'),
    path('upload_game_document/<str:game_name>', gestion_views.upload_game_document, name='upload_game_document'),
    path('upload_game_image/<str:game_name>', gestion_views.upload_game_image, name='upload_game_image'),
    
    # Template part
    path('dashboard/<str:game_name>/npc/create_template', gestion_views.create_npc_template, name="create_npc_template"),
    path('npc_template_selected/<str:game_name>', gestion_views.npc_template_selected, name='npc_template_selected'),
    path('template_name', gestion_views.npc_template_name_check, name='npc_template_name_check'),
    path('delete_npctemplate', gestion_views.delete_npctemplate, name="delete_npctemplate"),
    
    # Note part
    path('create_detailed_note/<str:game_name>', gestion_views.create_detailed_note, name='create_detailed_note'),
    # full note
    path('suppress_note_content/<str:game_name>', gestion_views.suppress_note_content, name="suppress_note_content"),
    path('save_note_content/<str:game_name>', gestion_views.save_note_content, name="save_note_content"),
    # quick note
    path('note', gestion_views.save_note, name='save_note'),
    
    # Login / logout part and account part
    path('account/confirmation/<uuid:factory_id>', gestion_views.set_activated_status, name='acount_activation'),
    url(r'^logout/$', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='user_logout'),
    path('username_check', gestion_views.username_check, name='username_check'),
    path('register_user', gestion_views.register_user, name='register_user'),
    path('login_check', gestion_views.login_check, name='login_check'),
    path('email_check', gestion_views.email_check, name='email_check'),
    path('password_check', gestion_views.password_check, name='password_check'),
    
    # Theme part
    path('select_theme', gestion_views.select_theme, name='select_theme'),
    
    # Game content part
    path('edit_item_description_text/<str:game_name>/<int:pk>', gestion_views.edit_item_description_text, name='edit_item_description_text'),
    path('get_new_item_informations/<str:game_name>', gestion_views.get_new_item_informations, name="get_new_item_informations"),
    path('edit_item_effect_text/<str:game_name>/<int:pk>', gestion_views.edit_item_effect_text, name='edit_item_effect_text'),
    path('edit_location_text/<str:game_name>/<int:pk>', gestion_views.edit_location_text, name='edit_location_text'),
    path('edit_scene_text/<str:game_name>/<int:pk>', gestion_views.edit_scene_text, name='edit_scene_text'),
    path('edit_scenario_text/<str:game_name>/', gestion_views.edit_scenario_text, name='edit_scenario_text'),
    path('edit_chapter_text/<str:game_name>/', gestion_views.edit_chapter_text, name="edit_chapter_text"),
    path('delete_location/<str:game_name>', gestion_views.delete_location, name='delete_location'),
    path('create_location/<str:game_name>', gestion_views.create_location, name='create_location'),
    path('create_scenario/<str:game_name>', gestion_views.create_scenario, name='create_scenario'),
    path('delete_scenario/<str:game_name>', gestion_views.delete_scenario, name='delete_scenario'),
    path('create_chapter/<str:game_name>', gestion_views.create_chapter, name='create_chapter'),
    path('delete_chapter/<str:game_name>', gestion_views.delete_chapter, name='delete_chapter'),
    path('create_scene/<str:game_name>', gestion_views.create_scene, name='create_scene'),
    path('delete_scene/<str:game_name>', gestion_views.delete_scene, name='delete_scene'),
    path('create_item/<str:game_name>', gestion_views.create_item, name='create_item'),
    path('delete_item/<str:game_name>', gestion_views.delete_item, name='delete_item'),
    path('check_form_value', gestion_views.check_form_value, name="check_form_value"),
    
    
    # Game part
    path('delete_game/<str:game_name>', gestion_views.delete_game, name='delete_game'),
    path('create_game', gestion_views.create_game, name='create_game'),
    path('update_game', gestion_views.update_game, name='update_game'),
    
    
    #npc
    path('delete_npc_from_location/<str:game_name>', gestion_views.delete_npc_from_location, name='delete_npc_from_location'),
    path('add_npc_in_location/<str:game_name>/<int:pk>', gestion_views.add_npc_in_location, name='add_npc_in_location'),
    path('delete_npc_from_scene/<str:game_name>', gestion_views.delete_npc_from_scene, name='delete_npc_from_scene'),
    path('add_npc_in_scene/<str:game_name>/<int:pk>', gestion_views.add_npc_in_scene, name='add_npc_in_scene'),
    path('npc_edit_sheet/<str:game_name>', gestion_views.npc_edit_sheet, name='npc_edit_sheet'),
    path('create_npc/<str:game_name>', gestion_views.create_npc, name='create_npc'),
    path('update_npc/<str:game_name>', gestion_views.update_npc, name='update_npc'),
    path('delete_npc/<str:game_name>', gestion_views.delete_npc, name='delete_npc'),
    path('npc_name', gestion_views.npc_template_name_check, name='npc_name_check'),
    
    #location
    path('delete_location_from_scene/<str:game_name>', gestion_views.delete_location_from_scene, name='delete_location_from_scene'),
    path('add_location_in_scene/<str:game_name>/<int:pk>/', gestion_views.add_location_in_scene, name='add_location_in_scene'),
    

]


generator_pattern = [
    # account part
    path('account/user/activation/user_already_active', generator_views.index, name='index_view_user_is_already_active'),
    path('account/user/create/error', generator_views.create_user_view, name='index_view_account_created_error'), 
    path('account/user/activation/user_not_found', generator_views.index, name='index_view_user_not_found'),
    path('account/user/activation/success', generator_views.index, name='index_view_user_is_active'),
    path('account/user/create/fail', generator_views.create_user_view, name='create_account_failed'),
    path('account/user/login/fail',  generator_views.index, name='index_view_account_login_error'),
    path('account/user/create/success', generator_views.index, name='index_view_account_created'),
    path('account/user/create/new',  generator_views.create_user_view, name='create_user_view'),
    
    
    # dashboard part
    path('dashboard', generator_views.dashboard, name='dashboard_view'),
    
    # dashboard part - fatal error
    
    path('ingame/<str:game_name>/dashboard/creation/failed/name_already_taken',  generator_views.dashboard, name='create_game_view_fail_image_already_exist'),
    path('ingame/<str:game_name>/dashboard/creation/failed/invalid_image_format',  generator_views.dashboard, name='upload_game_image_view_fail_image_ext'),
    path('ingame/<str:game_name>/dashboard/change_theme/failed/unknown_stylesheet',  generator_views.dashboard, name='dashboard_stylesheet_unknown_view'),
    path('ingame/<str:game_name>/dashboard/creation/failed/invalid_image_format',  generator_views.dashboard, name='create_game_view_fail_file_ext'),
    path('ingame/<str:game_name>/dashboard/scenario/creation/failed/unknown_game_error',generator_views.dashboard, name='game_doesnt_exist_view'),
    path('ingame/<str:game_name>/dashboard/npc_template/delete/failed/unknown',  generator_views.dashboard, name='delete_npc_template_fail'),
    path('ingame/<str:game_name>/dashboard/pictures/edit/fatal_error',  generator_views.dashboard, name='update_image_fatal_error'),
    path('ingame/<str:game_name>/dashboard/creation/success',  generator_views.dashboard, name='create_game_view_success'),
    path('ingame/<str:game_name>/dashboard/failed/fatal_error', generator_views.dashboard, name='failed_fatal_error_view'),
    path('dashboard/edition/failed/unknown_file_or_game',  generator_views.dashboard, name='update_game_view_failed'),
    path('dashboard/edition/success',  generator_views.dashboard, name='update_game_view_success'),
    
    # game part
    path('ingame/<str:game_name>/document/edit/fatal_error', generator_views.display_game_gallery_pictures, name='update_document_fatal_error'),
    path('ingame/<str:game_name>/upload/document/success', generator_views.display_game_content, name='upload_game_document_view_success'),
    path('ingame/<str:game_name>/dashboard/npc/creation/success',  generator_views.display_game_content, name='npc_creation_success_view'),
    path('ingame/<str:game_name>/upload/image/success', generator_views.display_game_content, name='upload_game_image_view_success'),
    path('ingame/<str:game_name>/content/npc-created',  generator_views.display_game_content, name='npc_created_game_content_view'),
    path('ingame/<str:game_name>/content',  generator_views.display_game_content, name='game_content_view'),
    path('ingame/<str:game_name>/edit',  generator_views.update_game_view, name='update_game_view'),
    path('dashboard/delete/success', generator_views.dashboard, name='delete_game_view_success'),
    path('dashboard/create',  generator_views.create_game_view, name='create_game_view'),
    
    # game document part
    path('ingame/<str:game_name>/upload/document/failed/invalid_document_format', generator_views.display_upload_game_document, name='upload_game_document_view_fail_image_ext'),
    path('ingame/<str:game_name>/document/display/<int:pk>', generator_views.display_selected_document, name='display_selected_document_view'),
    path('ingame/<str:game_name>/display/document/edit/unknown_document', generator_views.display_document_list_view, name='update_document_unknown'),
    path('ingame/<str:game_name>/display/document/edit/success', generator_views.display_document_list_view, name='update_document_success'),
    path('ingame/<str:game_name>/upload/document/success', generator_views.display_game_content, name='upload_game_document_view_success'),
    path('ingame/<str:game_name>/display/document/edit/fail', generator_views.display_document_list_view, name='update_document_fail'),
    path('ingame/<str:game_name>/upload/document', generator_views.display_upload_game_document, name='upload_game_document_view'),
    path('ingame/<str:game_name>/document/', generator_views.display_document_list_view, name='display_document_list_view'),
    
    # scenario part
    path('ingame/<str:game_name>/scenario/creation/failed/name_already_taken',generator_views.create_scenario_view, name='scenario_creation_fail_name_view'),
    path('ingame/<str:game_name>/scenario/creation/failed/fatal_error',generator_views.create_scenario_view, name='scenario_creation_fatal_error_view'),
    path('ingame/<str:game_name>/scenario/<int:pk>', generator_views.display_selected_scenario_view, name='display_selected_scenario_view'),
    path('ingame/<str:game_name>/scenario/creation/success',generator_views.display_game_content, name='scenario_creation_success_view'),
    path('ingame/<str:game_name>/scenario', generator_views.display_scenario_list_view, name='display_scenario_list_view'),
    path('ingame/<str:game_name>/scenario/create', generator_views.create_scenario_view, name='create_scenario_view'),
    path('', generator_views.index, name='index_view'),
    
    # chapter part
    path('ingame/<str:game_name>/chapter/creation/failed/fatal_error',generator_views.create_chapter_view, name='chapter_creation_fatal_error_view'),
    path('ingame/<str:game_name>/chapter/<int:pk>', generator_views.display_selected_chapter_view, name='display_selected_chapter_view'),
    path('ingame/<str:game_name>/chapter/creation/success',generator_views.display_game_content, name='chapter_creation_success_view'),
    path('ingame/<str:game_name>/chapter/creation/fail', generator_views.create_chapter_view, name='create_chapter_fail_view'),
    path('ingame/<str:game_name>/chapter', generator_views.display_chapter_list_view, name='display_chapter_list_view'),
    path('ingame/<str:game_name>/chapter/create', generator_views.create_chapter_view, name='create_chapter_view'),
    
    #scene part
    path('ingame/<str:game_name>/scene/<int:pk>', generator_views.display_selected_scene_view, name='display_selected_scene_view'),
    path('ingame/<str:game_name>/scene/creation/success',generator_views.display_game_content, name='scene_creation_success_view'),
    path('ingame/<str:game_name>/scene/creation/fail', generator_views.create_scene_view, name='scene_chapter_fail_view'),
    path('ingame/<str:game_name>/scene', generator_views.display_scene_list_view, name='display_scene_list_view'),
    path('ingame/<str:game_name>/scene/create', generator_views.create_scene_view, name='create_scene_view'),
    
    # npc part
    path('ingame/<str:game_name>/npc/template/create/error/name_already_exists', generator_views.create_npc_template_view, name='create_npc_template_name_already_exists'),
    path('ingame/<str:game_name>/npc/template/creation/success', generator_views.display_game_content, name='create_npc_template_view_success'),
    path('ingame/<str:game_name>/npc/template/select', generator_views.npc_template_selection, name='npc_template_selection_view'),
    path('ingame/<str:game_name>/npc/template/create', generator_views.create_npc_template_view, name='create_npc_template_view'),
    path('ingame/<str:game_name>/template-<str:template_id>/npc/create', generator_views.create_npc_view, name='create_npc_view'),
    path('ingame/<str:game_name>/npc/<int:pk>', generator_views.display_selected_npc_view, name='display_selected_npc_view'),
    path('ingame/<str:game_name>/npc', generator_views.display_npc_list_view, name='display_npc_list_view'),
    
    # item part
    path('ingame/<str:game_name>/item/<int:pk>', generator_views.display_selected_item_view, name='display_selected_item_view'),
    path('ingame/<str:game_name>/item/creation/success',generator_views.display_game_content, name='item_creation_success_view'),
    path('ingame/<str:game_name>/item/creation/fail', generator_views.create_item_view, name='create_item_fail_view'),
    path('ingame/<str:game_name>/item', generator_views.display_item_list_view, name='display_item_list_view'),
    path('ingame/<str:game_name>/item/create', generator_views.create_item_view, name='create_item_view'),
    
    # location part
    path('ingame/<str:game_name>/location/<int:pk>', generator_views.display_selected_location_view, name='display_selected_location_view'),
    path('ingame/<str:game_name>/location/creation/success',generator_views.display_game_content, name='location_creation_success_view'),
    path('ingame/<str:game_name>/location/creation/fail', generator_views.create_location_view, name='create_location_fail_view'),
    path('ingame/<str:game_name>/location', generator_views.display_location_list_view, name='display_location_list_view'),
    path('ingame/<str:game_name>/location/create', generator_views.create_location_view, name='create_location_view'),
    
    # note part
    path('ingame/<str:game_name>/note/creation/success',generator_views.display_game_content, name='note_creation_success_view'),
    path('ingame/<str:game_name>/note/creation/fail', generator_views.create_note_view, name='note_creation_fail_view'),
    path('ingame/<str:game_name>/note/create', generator_views.create_note_view, name='create_note_view'),
    path('ingame/<str:game_name>/note', generator_views.display_note_list_view, name='display_note_list_view'),
    
    # search content part
    path('ingame/<str:game_name>/search_result/<str:search_keyword>/<str:search_word>', generator_views.display_all_research_result, name='display_all_research_result_view'),
    
    #overview part
    path('<str:game_name>/overview',generator_views.display_overview_view, name='display_overview_view'),
    

]

#include use var_pattern list ; 'gestion' (ou gesmjgenerator) are app_name, and namespace is a ... namespace.
urlpatterns = [
    path('', include((generator_pattern, 'gesmjgenerator'), namespace="gesmjgenerator")),
    path('', include((gestion_pattern, 'gestion'), namespace="gestion")),
    path('email', include(mail_urls)),
    path('admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) to include media path.

# path('email', include(mail_urls)), original and working url.
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'password', 'last_login', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined')


class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_at', 'updated_at', 'deleted_at')


class ScenarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'id_game_id', 'description', 'created_at', 'updated_at', 'deleted_at')


class PargenAdmin(admin.ModelAdmin):
    list_display = ('id', 'pargen_type', 'value', 'created_at', 'updated_at', 'deleted_at')


class NpcAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'npc_statistics', 'npc_background', 'id_template_id_id', 'created_at', 'updated_at', 'deleted_at')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'image', 'id_game_id', 'description', 'created_at', 'updated_at', 'deleted_at')


class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'image', 'id_game_id', 'description', 'created_at', 'updated_at', 'deleted_at')


class ChapterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'image', 'order', 'description', 'resume', 'note', 'content', 'id_scenario_id', 'created_at', 'updated_at', 'deleted_at')


class SceneAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'image', 'order', 'description', 'id_chapter_id', 'created_at', 'updated_at', 'deleted_at')


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'short_name', 'owner_uuid', 'id_pargen_type_id_id', 'file', 'created_at', 'updated_at', 'deleted_at')
    
class KeywordAdmin(admin.ModelAdmin):
    list_display = ('id','id_game_id_id', 'id_document_id_id', 'id_pargenKeyword_id_id', 'created_at', 'updated_at', 'deleted_at')
    
class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner_uuid_id','id_game_id_id', 'note', 'created_at', 'updated_at', 'deleted_at')


admin.site.register(User, UserAdmin)
admin.site.register(Pargen, PargenAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(Scenario, ScenarioAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Chapter, ChapterAdmin)
admin.site.register(Scene, SceneAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Keyword, KeywordAdmin)
admin.site.register(Note, NoteAdmin)
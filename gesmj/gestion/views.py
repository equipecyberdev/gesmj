
# -*- coding: utf-8 -*-

import json
import pathlib
import random

import fitz
from django.apps import apps
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_protect, csrf_exempt
from django_email_verification import *

from gesmjgenerator.forms import *
from gestion.classes.update_db import UpdateDb
from .classes.file_gestion import FileGestion
from .classes.get_from_db import GetFromDb
from .models import *


@csrf_exempt
def login_check(request):
    """
    :param request:
    :return:
    """
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            this_user = User.objects.filter(username=username, password=password).values()
            if user is not None:
                login(request, user)
                return redirect('gesmjgenerator:dashboard_view')
            else:
                loginForm = LoginForm()
                response_data = "Ce nom de compte ou mot de passe est invalide."
                return HttpResponseRedirect(reverse('gesmjgenerator:index_view_account_login_error'), { 'loginForm': loginForm, 'error_msg' : response_data})
        else:
            return redirect('gesmjgenerator:index_view')
    else:
        return redirect('gesmjgenerator:index_view')
    
@ensure_csrf_cookie
@csrf_protect
def password_check(request):
    response_data = {}
    if request.method == 'POST':
        password = request.POST.get('password')
        if not re.match("^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*_.,?!;#?&])[A-Za-z\d@$!_.,;?!#%*?&]{8,150}$", password):
            response_data = {'error': True}
        else:
            response_data = {'success': True}
        return JsonResponse(response_data)
    
@ensure_csrf_cookie
@csrf_protect
def register_user(request): 
    response_data = {}
    loginForm = LoginForm()
    if request.method == 'POST':
        User = get_user_model()
        register_form = UserCreateForm(request.POST)
        if register_form.is_valid() and (request.POST.get('password1') == request.POST.get('password2')):
            if not User.objects.filter(username=register_form.cleaned_data.get('username')).exists() and not User.objects.filter(email=register_form.cleaned_data.get('email')).exists():
                new_user = register_form.save(commit=False)
                new_user.is_active = True
                new_user.save()
                response_data = 'Votre compte a été créer avec succès, vous pouvez maintenant vous connecter.'
                return HttpResponseRedirect(reverse('gesmjgenerator:index_view_account_created'), { 'loginForm': loginForm, 'confirmation_msg': response_data })
            else:
                response_data = 'Votre login ou votre adresse email existe déjà.'
                return HttpResponseRedirect(reverse('gesmjgenerator:index_view_account_created_error'), { 'loginForm': loginForm, 'register_form':register_form, 'error_msg':response_data })
        else:
            response_data = "Votre mot de passe doit contenir entre 8 et 150 caractères. Au moins un caractère spécial: '$', '@', '#', '%', '_', '?', '!', ';', ',' '.' un minimum de une majuscule et enfin au moins un chiffre"
            return HttpResponseRedirect(reverse('gesmjgenerator:index_view_account_created_error'), { 'loginForm': loginForm, 'register_form':register_form,  'error_msg':response_data })
    else:
        loginForm = LoginForm()
        response_data = {"error": True, "message": "La création de votre compte n'a pu aboutir, veuillez réessayer ou contacter l'administrateur du site."}
        return HttpResponseRedirect(reverse('gesmjgenerator:index_view_account_created_error'), { 'loginForm': loginForm })
    
    
@ensure_csrf_cookie
@csrf_protect
def username_check(request):
    response_data = {}
    if request.method == 'POST':
        new_username = request.POST.get('username')
        if User.objects.filter(username=new_username).exists():
            response_data = {'error': True}
        else:
            response_data = {'success': True}
        return JsonResponse(response_data)
    
    
@ensure_csrf_cookie
@csrf_protect
def email_check(request):
    response_data = {}
    if request.method == 'POST':
        new_email = request.POST.get('email')
        if User.objects.filter(email=new_email).exists():
            response_data = {'error': True}
        else:
            response_data = {'success': True}
        return JsonResponse(response_data)
        
        
def set_activated_status(request, user_uuid):
    pass


@login_required
@ensure_csrf_cookie
@csrf_protect
def user_logout(request):
    if request.user.is_authenticated:
        Session.objects.get(session_key=request.session.session_key).delete()
        logout(request)
        return HttpResponse(settings.LOGOUT_REDIRECT_URL)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    

@login_required
@ensure_csrf_cookie
@csrf_protect
def create_game(request):
    if request.user.is_authenticated:
        response_data = {}
        type = ''
        if request.method == 'POST':
            # get request values in UploadFileForm
            form = UploadFileForm(request.POST, request.FILES)
            # check if form is okay
            if form.is_valid():
                # get game_name and original file name and replace whitespace.
                # regexp to rename and delete every not needed symbols
                # allow using "." and " ' " but not on start or end
                game_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)','-', request.POST.get('name'))
                game_name = re.sub(r"(^-+)|(^_+)|(^'+)|(^\.+)|(-$)|(_$)|('\.$)", '', game_name)
                response_data = '{}'.format(game_name)
                original_file_filename = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.FILES['file'].name)
                original_file_filename = re.sub(r"(^-+)|(^_+)|(^'+)|(^\.+)|(-$)|(_$)|('\.$)", '', original_file_filename)
                db = GetFromDb(request.user, game_name, "IMAGE")
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                    this_file = FileGestion(request.user.username, game_name, "IMAGE")
                    uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(original_file_filename, "")
                    this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename)
                    # create new document
                    if db.get_pargen_id(uuid_full_filename.lower()) is False:
                        response_data = {'error': True, 'msg':'Veuillez choisir un format d\'image correcte exemple, les formats corrects sont : <strong>jpg, png, svg</strong>.'}
                        return HttpResponseRedirect(reverse('gesmjgenerator:create_game_view_fail_image_ext'))
                    else:
                        # create new game
                        new_game = Game.objects.create(
                            name=game_name,
                            file= uuid_full_filename,
                            owner_uuid_id=request.user.id
                        ).save()
                        game_id = db.get_game_id()
                        # create new file in user game name directory 
                        Document.objects.create(
                            name=uuid_full_filename,
                            short_name=no_uuid_shortname,
                            id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            file=this_file.get_full_path()
                        ).save()
                        return HttpResponseRedirect(reverse('gesmjgenerator:create_game_view_success', args=[response_data]))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:create_game_view_fail_already_exist', args=[response_data]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:create_game_view_fail_file_ext', args=[response_data]))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def update_game(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            old_game_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('old_game_name'))
            old_game_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', old_game_name)
            # regexp to rename and delete every not needed symbols
            # allow using "." and " ' " but not on start or end
            updated_game_name = re.sub('/*[/^ \s/\W/g]', '-', request.POST.get('name'))
            updated_game_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', updated_game_name)
            if request.FILES:
                # file have been changed and need update.
                # if request.FILES is not empty
                old_file_name = request.POST.get('old_file_name')
                new_filename = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.FILES['file'].name)
                new_filename = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', new_filename)
                
                db = GetFromDb(request.user, old_game_name, "IMAGE")
                old_game_id = db.get_game_id()
                if db.get_pargen_id(new_filename):
                    # file gestion part 
                    old_file = FileGestion(request.user.username, old_game_name, "IMAGE")
                    # delete old one
                    old_file.delete_uploaded_file({'old_file_name': old_file_name})
                    # rename game directory
                    old_file.rename_game_directory(updated_game_name)
                    # create new file 
                    new_file = FileGestion(request.user.username, updated_game_name, "IMAGE")
                    uuid_full_filename, uuid_filename, no_uuid_shortname = new_file.set_uuid_on_filename(new_filename, "")
                    # Update game values
                    Game.objects.filter(name=old_game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).update(
                        name=updated_game_name,
                        file=uuid_full_filename
                    )
                    # update document table
                    Document.objects.filter(name=old_file_name, owner_uuid_id=request.user.id, id_game_id_id=old_game_id).update(
                        name=uuid_full_filename,
                        id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename)
                    )
                    new_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                    response_data = {"old_game_name": old_game_name, "updated_game_name": updated_game_name}
                    return JsonResponse(response_data)
                else:
                    response_data = "Le format de l'image que vous essayez d'utiliser n'est pas accepté.\nFormat d'images acceptés: <b>jpg, png</b>"
                    return JsonResponse(response_data)
            else:
                # file remain the same and don't need update / delete.
                if Game.objects.filter(name=old_game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    Game.objects.filter(name=old_game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).update(
                        name=updated_game_name.split('.')[0]
                    )
                    FileGestion(request.user.username, old_game_name, "IMAGE").rename_game_directory(updated_game_name)
                    response_data = "Votre jeu anciennement '{}' a correctement été modifié en '{}'".format(old_game_name, updated_game_name)
                    return JsonResponse(response_data)
                else:
                    response_data = "Le jeu que vous tentez de modifier n'existe pas..."
                    return JsonResponse(response_data)
        else:
            response_data = "Erreur critique, veuillez contacter l'administrateur du site..."
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            

@login_required
@ensure_csrf_cookie
@csrf_protect
def search_content_engine(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            response_data = {}
            game_name = request.POST.get('game_name')
            keyword = request.POST.get('keyword')
            search_word = request.POST.get('search_word')
            keyword_list = ['scenario', 'chapter', 'scene', 'item', 'npc', 'document', 'location']
            if keyword in keyword_list:
                if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                    search_in_db = GetFromDb(request.user.id, game_id[0]).search_in_db_engine(search_word, keyword)
                    if search_in_db is None:
                        response_data = {"no_result":True}
                    else:
                        response_data = {"data_information" : search_in_db}
                    return JsonResponse(response_data)
            else:
                response_data = {'bad_or_no_keyword':True}
                return JsonResponse(response_data)

@login_required
@ensure_csrf_cookie
@csrf_protect
def create_scenario(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            form = ScenarioCreateForm(request.POST, request.FILES)
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    document_can_be_created = False
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    scenario_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', name)
                    db = GetFromDb(request.user, game_name, "IMAGE")
                    game_id = db.get_game_id()
                    if not Scenario.objects.filter(name=scenario_name, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).exists():
                        if request.FILES:
                            document_can_be_created = True
                            original_image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-',  request.FILES['file'].name)
                            image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', original_image_name) 
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(image_name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # get game_id
                            # create new document
                            if db.get_pargen_id(uuid_full_filename) is False:
                                return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                            else:
                                # create new document for selectionned game.
                                Document.objects.create(
                                    name=uuid_full_filename,
                                    short_name=no_uuid_shortname,
                                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                    owner_uuid_id=request.user.id,
                                    id_game_id_id=game_id,
                                    file=this_file.get_full_path()
                                ).save()
                        
                        Scenario.objects.create(
                            name=scenario_name,
                            description=request.POST.get('description'),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            image= uuid_full_filename if document_can_be_created is True else None
                        ).save()
                        if document_can_be_created is True:
                            document_id = Document.objects.filter(name=uuid_full_filename, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            scenario_id = Scenario.objects.filter(name=scenario_name, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            simple_game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                            DocumentInScenario.objects.create(
                                id_game_id_id=simple_game_id[0],
                                id_document_id_id=document_id[0],      
                                id_scenario_id_id=scenario_id[0] 
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='scenario', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id =simple_game_id[0],
                                id_document_id_id = document_id[0]
                            ).save()
                    
                        return HttpResponseRedirect(reverse('gesmjgenerator:scenario_creation_success_view', args=[game_name]))
                    else:
                        return HttpResponseRedirect(reverse('gesmjgenerator:scenario_creation_fail_name_view', args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            
            
@login_required
@ensure_csrf_cookie
@csrf_protect
def scenario_check_name_exists(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == "POST":
            name = request.POST['name']
            if Scenario.objects.filter(name=name, id_game_id_id=request.session['selected_game_id']).exists():
                response_data = {'exists': True}
            else:
                response_data = {'exists': False}
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
        
        
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_scenario_text(request, game_name):
    response_data = {}
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                scenario_id = int(request.POST.get('scenario_id'))
                new_text = request.POST.get('description')
                if Scenario.objects.filter(id_game_id_id=game_id[0], id=scenario_id, owner_uuid_id=request.user.id).exists():
                    this_scenario = Scenario.objects.get(id_game_id_id=game_id[0], id=scenario_id, owner_uuid_id=request.user.id)
                    this_scenario.description = new_text
                    this_scenario.save()
                return JsonResponse(response_data)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_scenario(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
            else:
                scenario_name_list = [ key for key, value in json.loads(request.POST['scenario_informations']).items()]
                scenario_id_list = [ value for key, value in json.loads(request.POST['scenario_informations']).items() ]
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id',flat=True)
                if Scenario.objects.filter(name__in=scenario_name_list, id__in=scenario_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    # get id
                    chapters_id = Chapter.objects.filter(id_scenario_id_id__in=scenario_id_list, owner_uuid_id=request.user.id).values('id')
                    scenes_id = Scene.objects.filter(id_chapter_id_id__in=chapters_id, owner_uuid_id=request.user.id).values('id')
                    # get all file_name 
                    s_files = Scenario.objects.filter(id__in=scenario_id_list, owner_uuid_id=request.user.id, id_game_id_id=game_id[0], deleted_at__isnull=True).values('image')
                    c_files = Chapter.objects.filter(id_scenario_id_id__in=scenario_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).values('image')
                    sc_files = Scene.objects.filter(id_chapter_id_id__in=chapters_id, owner_uuid_id=request.user.id).values('image')
                    scenarios_files = {str(uuid.uuid4()): entry['image'] for entry in s_files if not entry['image'] is None}
                    chapters_files = {str(uuid.uuid4()): entry['image'] for entry in c_files if not entry['image'] is None}
                    scenes_files = {str(uuid.uuid4()): entry['image'] for entry in sc_files if not entry['image'] is None}
                    all_files = {}
                    all_files.update(scenarios_files)
                    all_files.update(chapters_files)
                    all_files.update(scenes_files)
                    all_files_only_name = [ value for value in all_files.values()]
                    # db entry gestion part
                    ItemInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scenes_id).delete()
                    LocationInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scenes_id).delete()
                    DocumentInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scenes_id).delete()
                    NpcInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scenes_id).delete()
                    Scene.objects.filter(id_chapter_id_id__in=chapters_id, owner_uuid_id=request.user.id).delete()
                    ItemInChapter.objects.filter(id_game_id_id=game_id[0], id_chapter_id_id__in=scenes_id).delete()
                    LocationInChapter.objects.filter(id_game_id_id=game_id[0], id_chapter_id_id__in=scenes_id).delete()
                    DocumentInChapter.objects.filter(id_game_id_id=game_id[0], id_chapter_id_id__in=scenes_id).delete()
                    NpcInChapter.objects.filter(id_game_id_id=game_id[0], id_chapter_id_id__in=scenes_id).delete()
                    Chapter.objects.filter(id__in=chapters_id, owner_uuid_id=request.user.id).delete()
                    ItemInScenario.objects.filter(id_game_id_id=game_id[0], id_scenario_id_id__in=scenes_id).delete()
                    LocationInScenario.objects.filter(id_game_id_id=game_id[0], id_scenario_id_id__in=scenes_id).delete()
                    DocumentInScenario.objects.filter(id_game_id_id=game_id[0], id_scenario_id_id__in=scenes_id).delete()
                    NpcInScenario.objects.filter(id_game_id_id=game_id[0], id_scenario_id_id__in=scenes_id).delete()
                    Scenario.objects.filter(id__in=scenario_id_list, owner_uuid=request.user.id, id_game_id_id=game_id[0]).delete()
                    Document.objects.filter(name__in=all_files_only_name, id_game_id_id=game_id[0], owner_uuid_id=request.user.id).delete()
                    # file gestion part 
                    file = FileGestion(request.user.username, game_name, "IMAGE")
                    file.delete_uploaded_file(all_files)
                    
                    
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
         
  


@login_required
@ensure_csrf_cookie
@csrf_protect
def create_chapter(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            form = ChapterCreateForm(request.POST, request.FILES)
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    document_can_be_created = False
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    chapter_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', name)
                    if not Chapter.objects.filter(name=chapter_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists() and not Chapter.objects.filter(order=int(request.POST.get('order')), id_scenario_id_id=int(request.POST.get('scenario')), owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                        if request.FILES:
                            document_can_be_created = True
                            original_image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-',  request.FILES['file'].name)
                            image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', original_image_name) 
                            
                            db = GetFromDb(request.user, game_name, "IMAGE")
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(image_name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # get game_id
                            game_id = db.get_game_id()
                            # create new document
                            if db.get_pargen_id(uuid_full_filename) is False:
                                return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                            else:
                                # create new document for selectionned game.
                                Document.objects.create(
                                    name=uuid_full_filename,
                                    short_name=no_uuid_shortname,
                                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                    owner_uuid_id=request.user.id,
                                    id_game_id_id=game_id,
                                    file=this_file.get_full_path()
                                ).save()
                        
                        Chapter.objects.create(
                            name=chapter_name,
                            description=request.POST.get('description'),
                            order=int(request.POST.get('order')),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            id_scenario_id_id=int(request.POST.get('scenario')),
                            image= uuid_full_filename if document_can_be_created is True else None
                        ).save()
                        if document_can_be_created is True:
                            document_id = Document.objects.filter(name=uuid_full_filename, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            chapter_id = Chapter.objects.filter(name=chapter_name, owner_uuid_id=request.user.id, id_scenario_id_id=int(request.POST.get('scenario'))).values_list('id', flat=True)
                            simple_game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                            DocumentInChapter.objects.create(
                                id_game_id_id=simple_game_id[0],
                                id_document_id_id=document_id[0],      
                                id_chapter_id_id=chapter_id[0] 
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='chapitre', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id =simple_game_id[0],
                                id_document_id_id = document_id[0]
                            ).save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:chapter_creation_success_view', args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:chapter_creation_fatal_error_view'))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_chapter(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
            else:
                chapter_name_list = [ key for key, value in json.loads(request.POST['chapter_informations']).items()]
                chapter_id_list = [ value for key, value in json.loads(request.POST['chapter_informations']).items() ]
                scene_id_list = Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id__in=chapter_id_list, deleted_at__isnull=True).values('id')
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                    
                if Chapter.objects.filter(id__in=chapter_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    DocumentInChapter.objects.filter(id_game_id_id=game_id[0], id_chapter_id_id__in=chapter_id_list).delete()
                    document_list = Chapter.objects.filter(id__in=chapter_id_list, owner_uuid_id=request.user.id).values('image')
                    document_name_list = [entry['image'] for entry in document_list]
                    get_chapter_filename_queryset = Chapter.objects.filter(id__in=chapter_id_list, owner_uuid_id=request.user.id).values('id','image')
                    get_chapter_filename_list = {}
                    for value in get_chapter_filename_queryset:
                        if not value['image'] is None:
                            get_chapter_filename_list.update({value['id']:value['image']})
                    # file gestion part 
                    file = FileGestion(request.user.username, game_name, "IMAGE")
                    # delete scene link first
                    if not scene_id_list is None: 
                        get_scene_filename_queryset = Scene.objects.filter(id__in=scene_id_list, owner_uuid_id=request.user.id).values('id','image')
                        get_scene_filename_list = {}
                        for value in get_scene_filename_queryset:
                            if not value['image'] is None:
                                get_scene_filename_list.update({value['id']: value['image']})
                        file.delete_uploaded_file(get_scene_filename_list)
                        DocumentInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scene_id_list).delete()
                        Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id__in=chapter_id_list).delete()
                    # delete chapter
                    file.delete_uploaded_file(get_chapter_filename_list)
                    Chapter.objects.filter(id__in=chapter_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    Document.objects.filter(name__in=document_name_list, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_chapter_text(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                chapter_description = request.POST.get('description')
                chapter_id = request.POST.get('chapter_id') 
                if Chapter.objects.filter(id=int(chapter_id), owner_uuid_id=request.user.id).exists():
                    this_chapter = Chapter.objects.get(id=int(chapter_id), owner_uuid_id=request.user.id)
                    this_chapter.description = chapter_description
                    this_chapter.save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_chapter_view',kwargs={'game_name': game_name, 'chapter_id': int(chapter_id)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
         
  
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_scene(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            form = SceneCreateForm(request.POST, request.FILES)
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    document_can_be_created = False
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    scene_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', name)
                    if not Scene.objects.filter(name=scene_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists() and not Scene.objects.filter(order=int(request.POST.get('order')), id_chapter_id_id=int(request.POST.get('chapter')), owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                        if request.FILES:
                            document_can_be_created = True
                            original_image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-',  request.FILES['file'].name)
                            image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', original_image_name) 
                            db = GetFromDb(request.user, game_name, "IMAGE")
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(image_name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # get game_id
                            game_id = db.get_game_id()
                            # create new document
                            if db.get_pargen_id(uuid_full_filename) is False:
                                return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                            else:
                                # create new document.
                                Document.objects.create(
                                    name=uuid_full_filename,
                                    short_name=no_uuid_shortname,
                                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                    owner_uuid_id=request.user.id,
                                    id_game_id_id=game_id,
                                    file=this_file.get_full_path()
                                ).save()
                        
                        Scene.objects.create(
                            name=scene_name,
                            description=request.POST.get('description'),
                            order=int(request.POST.get('order')),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            id_chapter_id_id=int(request.POST.get('chapter')),
                            image=uuid_full_filename if document_can_be_created is True else None
                        ).save()
                        if document_can_be_created is True:
                            document_id = Document.objects.filter(name=uuid_full_filename, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            scene_id = Scene.objects.filter(name=scene_name, owner_uuid_id=request.user.id, id_chapter_id_id=int(request.POST.get('chapter'))).values_list('id', flat=True)
                            simple_game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                            DocumentInScene.objects.create(
                                id_game_id_id=simple_game_id[0],
                                id_document_id_id=document_id[0],      
                                id_scene_id_id=scene_id[0] 
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='scene', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id =simple_game_id[0],
                                id_document_id_id = document_id[0]
                            ).save()
                        return HttpResponseRedirect(reverse('gesmjgenerator:scene_creation_success_view', args=[game_name]))
                    else:
                        return HttpResponseRedirect(reverse('gesmjgenerator:create_scene_fail_view', args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:scene_creation_fatal_error_view', args=[game_name]))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_scene(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
            else:
                scene_name_list = [ key for key, value in json.loads(request.POST['scene_informations']).items()]
                scene_id_list = [ value for key, value in json.loads(request.POST['scene_informations']).items() ]
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                if Scene.objects.filter(id__in=scene_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    DocumentInScene.objects.filter(id_game_id_id=game_id[0], id_scene_id_id__in=scene_id_list).delete()
                    document_list = Scene.objects.filter(id__in=scene_id_list, owner_uuid_id=request.user.id).values('image')
                    document_name_list = [entry['image'] for entry in document_list]
                    get_scene_filename_queryset = Scene.objects.filter(id__in=scene_id_list, owner_uuid_id=request.user.id).values('id','image')
                    get_scene_filename_list = {}
                    for value in get_scene_filename_queryset:
                        if not value['image'] is None:
                            get_scene_filename_list.update({value['id']:value['image']})
                    # file gestion part 
                    file = FileGestion(request.user.username, game_name, "IMAGE")
                    # delete old one
                    file.delete_uploaded_file(get_scene_filename_list)
                    Scene.objects.filter(name__in=scene_name_list, id__in=scene_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    Document.objects.filter(name__in=document_name_list, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            
                    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_item(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            form = ItemCreateForm(request.POST, request.FILES)
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    document_can_be_created = False
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    item_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', name)
                    if not Item.objects.filter(name=item_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                        if request.FILES:
                            document_can_be_created = True
                            original_image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-',  request.FILES['file'].name)
                            image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', original_image_name) 
                            db = GetFromDb(request.user, game_name, "IMAGE")
                            game_id = db.get_game_id()
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(image_name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # get game_id
                            game_id = db.get_game_id()
                            # create new document
                            if db.get_pargen_id(uuid_full_filename) is False:
                                return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                            else:
                                # create new document for selectionned game.
                                Document.objects.create(
                                    name=uuid_full_filename,
                                    short_name=no_uuid_shortname,
                                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                    owner_uuid_id=request.user.id,
                                    id_game_id_id=game_id,
                                    file=this_file.get_full_path()
                                ).save()
                        
                        Item.objects.create(
                            name=item_name,
                            formated_name=request.POST.get('name'),
                            description=request.POST.get('description'),
                            effect=request.POST.get('effect'),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            image= uuid_full_filename if document_can_be_created is True else None
                        ).save()
                        if document_can_be_created is True:
                            document_id = Document.objects.filter(name=uuid_full_filename, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            simple_game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                            item_id = Item.objects.filter(name=item_name, owner_uuid_id=request.user.id, id_game_id_id=simple_game_id[0]).values_list('id', flat=True)
                            DocumentInItem.objects.create(
                                id_game_id_id=simple_game_id[0],
                                id_document_id_id=document_id[0],      
                                id_item_id_id=item_id
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='objet', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id =simple_game_id[0],
                                id_document_id_id = document_id[0]
                            ).save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:item_creation_success_view', args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    

@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_item(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
            else:
                item_name_list = [ key for key, value in json.loads(request.POST['item_informations']).items()]
                item_id_list = [ value for key, value in json.loads(request.POST['item_informations']).items() ]
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                if Item.objects.filter(id__in=item_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    DocumentInItem.objects.filter(id_game_id_id=game_id[0], id_item_id_id__in=item_id_list).delete()
                    item_list = Item.objects.filter(id__in=item_id_list, owner_uuid_id=request.user.id).values('image')
                    item_name_list = [entry['image'] for entry in item_list]
                    get_item_filename_queryset = Scene.objects.filter(id__in=item_id_list, owner_uuid_id=request.user.id).values('id','image')
                    get_item_filename_list = {}
                    
                    for value in get_item_filename_queryset:
                        if not value['image'] is None:
                            get_item_filename_list.update({value['id']:value['image']})
                    
                    # set a "set" list type, to store every item id in singleton
                    npc_id_linked_with_item_id = set(ItemInNpc.objects.filter(id_item_id_id__in=item_id_list, id_game_id_id=game_id[0]).values_list('id_npc_id_id', flat=True))
                    # get npc by id
                    npc_inventory = Npc.objects.filter(id__in=npc_id_linked_with_item_id, owner_uuid_id=request.user.id)
                    # get inventory table content
                    npc_inventory_item_fully_listed = npc_inventory.values_list('inventory', flat=True)
                    # pack data to use it in for loop.
                    npc_inventory_item_fully_listed_and_packed = [entry for entry in npc_inventory_item_fully_listed]
                    inventory_dict = {}
                    for npc in npc_inventory:
                        for i in range(0,len(npc_inventory_item_fully_listed_and_packed)):
                            # get updated item list
                            inventory_dict.update({slot_name: image_name for slot_name, image_name in npc_inventory_item_fully_listed_and_packed[i].items() if not image_name in item_name_list})
                            # update inventory
                            npc.inventory = inventory_dict
                            npc.save()
                            
                    # file gestion part 
                    file = FileGestion(request.user.username, game_name, "IMAGE")
                    # delete old one
                    file.delete_uploaded_file(get_item_filename_list)
                    # delete db entry
                    DocumentInItem.objects.filter(id_item_id_id__in=item_id_list, id_game_id_id=game_id[0]).delete()
                    ItemInNpc.objects.filter(id_game_id_id=game_id[0], id_item_id_id__in=item_id_list).delete()
                    Item.objects.filter(id__in=item_id_list, owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    Document.objects.filter(name__in=item_name_list, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).delete()
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
            
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_location(request, game_name):
    response_data = {}
    if request.user.is_authenticated: 
        if request.method == 'POST':
            form = LocationCreateForm(request.POST, request.FILES)
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    document_can_be_created = False
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    location_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', name)
                    if not Location.objects.filter(name=location_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                        if request.FILES:
                            document_can_be_created = True
                            original_image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-',  request.FILES['file'].name)
                            image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', original_image_name) 
                            db = GetFromDb(request.user, game_name, "IMAGE")
                            game_id = db.get_game_id()
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(image_name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # get game_id
                            game_id = db.get_game_id()
                            # create new document
                            if db.get_pargen_id(uuid_full_filename) is False:
                                return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                            else:
                                # create new document for selectionned game.
                                Document.objects.create(
                                    name=uuid_full_filename,
                                    short_name=no_uuid_shortname,
                                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                    owner_uuid_id=request.user.id,
                                    id_game_id_id=game_id,
                                    file=this_file.get_full_path()
                                ).save()
                        
                        Location.objects.create(
                            name=location_name,
                            description=request.POST.get('description'),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            image= uuid_full_filename if document_can_be_created is True else None
                        ).save()
                        if document_can_be_created is True:
                            document_id = Document.objects.filter(name=uuid_full_filename, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                            simple_game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                            location_id = Location.objects.filter(name=location_name, owner_uuid_id=request.user.id, id_game_id_id=simple_game_id[0]).values_list('id', flat=True)
                            DocumentInLocation.objects.create(
                                id_game_id_id=simple_game_id[0],
                                id_document_id_id=document_id[0],      
                                id_location_id_id=location_id
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='lieu', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id =simple_game_id[0],
                                id_document_id_id = document_id[0]
                            ).save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:location_creation_success_view', args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:location_creation_fatal_error_view'))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
  
@ensure_csrf_cookie
@csrf_protect
def check_form_value(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == "POST":
            name = request.POST.get('name')
            type = request.POST.get('type')
            if request.POST.get('category') == "user":
                response_data = {'exists': False}
                if type == "username":
                    if User.objects.filter(username=name).exists():
                        response_data = {'exists': True, 'response': 'Ce nom'}
                    else:
                        response_data = {'exists': False, 'response': 'Ce nom'}     
                elif type == "email":
                    if User.objects.filter(email=name).exists():
                        response_data = {'exists': True, 'response': 'Cette adresse email'}
                    else:
                        response_data = {'exists': False, 'response': 'Cette adresse email'}
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
        
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_npc_template(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == "POST":
            last_url = request.POST.get('next', '/')
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                competence_option = request.POST.get('template-skills-options') if ('number' in request.POST.get('template-skills-options') or 'far' in request.POST.get('template-skills-options')) else 'number'
                attribute_option = request.POST.get('template-attributes-options') if ('number' in request.POST.get('template-attributes-options') or 'far' in request.POST.get('template-attributes-options')) else 'number'
                if NpcTemplate.objects.filter(name=request.POST.get('template_name'), id_game_id_id=game_id[0]).exists() is False:
                    statistics = {'attributes_option': attribute_option, 
                                'attributes_values': request.POST.get('template-attributes-values'),
                                'attributes_names': request.POST.getlist('attributes[]'),
                                'skills_option': competence_option, 
                                'skills_values': request.POST.get('template-skills-values'),
                                'skills_names': request.POST.getlist('skills[]')
                            }
                    background = {'npc_informations': request.POST.getlist('informations[]'),
                                'npc_descriptions': request.POST.getlist('description[]'),
                                }
                    style = {'title_font': request.POST.get('title-font'),
                            'title_font_style_first': request.POST.get('title-font-style').split('-')[0],
                            'title_font_style_second': request.POST.get('title-font-style').split('-')[1],
                            'title_font_size': request.POST.get('title-font-size'),
                            'title_font_color': request.POST.get('title-font-color'),
                            'text_font': request.POST.get('text-font'),
                            'text_font_style_first': request.POST.get('text-font-style').split('-')[0],
                            'text_font_style_second': request.POST.get('text-font-style').split('-')[1],
                            'text_font_size': request.POST.get('text-font-size'),
                            'text_font_color': request.POST.get('text-font-color'),
                            'foreground_color': request.POST.get('template-foreground-color') if request.POST.get('template-foreground-color') != '#0000ff' else '',
                            'background_color': request.POST.get('template-font-color') if request.POST.get('template-font-color') != '#0000ff' else ''}
                    
                    NpcTemplate.objects.create(
                        owner_uuid_id=request.user.id,
                        id_game_id_id=game_id[0],
                        name=request.POST.get('template-name'),
                        statistics=statistics,
                        background=background,
                        style=style
                    ).save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:create_npc_template_view_success', kwargs={'game_name': game_name}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:create_npc_template_name_already_exists', kwargs={'game_name': game_name}))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_npctemplate(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            if NpcTemplate.objects.filter(id=request.POST.get('template_id'), owner_uuid_id=request.user.id).exists():
                this_template = NpcTemplate.objects.get(id=request.POST.get('template_id'), owner_uuid_id=request.user.id)
                this_template.delete()
                response_data = {'success' : True}
                return JsonResponse(response_data)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:delete_npc_template_fail', game_name=request.POST.get('game_name')))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[request.POST.get('game_name')]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
                
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def npc_template_selected(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            template_id = request.POST.get('template-select')
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists() and NpcTemplate.objects.filter(id=template_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                template_data = NpcTemplate.objects.filter(id=template_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id).values('statistics', 'style', 'name','background')
                return HttpResponseRedirect(reverse('gesmjgenerator:create_npc_view', kwargs={'game_name':game_name, 'template_id':template_id}))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    

@login_required
@ensure_csrf_cookie
@csrf_protect
def create_npc(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            last_url = request.POST.get('next', '/')
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                if NpcTemplate.objects.filter(id=request.POST.get('template_id'), owner_uuid_id=request.user.id).exists():
                    db = GetFromDb(request.user, game_name, "IMAGE")
                    game_id = db.get_game_id()
                    background_description = json.loads(request.POST['background_text'])
                    background_information = json.loads(request.POST['background_informations'])
                    existing_items = [{
                        'inventory_slot':key,
                        'image_name':value,
                        'id': list(Item.objects.filter(image=value, id_game_id_id=game_id, owner_uuid_id=request.user.id).values_list('id', flat=True))} for key, value in json.loads(request.POST['inventory_fields']).items()]
                    formated_inventory = [entry for entry in existing_items]
                    formatted_skills = {key: value for key, value in json.loads(request.POST['skills']).items() if key !='' and key != None }
                    formatted_attributes = {key: value for key, value in json.loads(request.POST['attributes']).items() if key !='' and key != None }
                    attributes = formatted_attributes
                    skills = formatted_skills
                    common_items = [{'name': key,'formatted_name':key.replace(' ', '_').replace("\'","_")+'_'+str(random.randint(1, 10000000)) , 'quantity': value, 'id': str(random.randint(1, 10000000)) } for key, value in json.loads(request.POST['common_items']).items() if key !='' and key != None ]
                    template_id = request.POST.get('template_id')
                    name = request.POST.get('npc_name')
                    inventory = json.loads(request.POST['inventory_fields'])
                    uuid_full_filename = ''
                    if request.FILES:
                        # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                        this_file = FileGestion(request.user.username, game_name, "IMAGE")
                        uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(request.FILES['file'].name, "")
                        this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                        # create new document for selectionned game.
                        Document.objects.create(
                            name=uuid_full_filename,
                            short_name=no_uuid_shortname,
                            id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            file=this_file.get_full_path()
                        ).save()
                        
                        
                    Npc.objects.create(
                        name=name.replace('"',''),
                        id_game_id_id=game_id,
                        owner_uuid_id=request.user.id,
                        id_template_id_id=template_id,
                        attributes=attributes,
                        skills=skills,
                        information=background_information,
                        description=background_description,
                        inventory=inventory,
                        common_item=common_items,
                        image=uuid_full_filename
                    ).save()
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})    
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def update_npc(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                npc_id = request.POST.get('npc_id') if request.POST.get('npc_id') != None and request.POST.get('npc_id') != '' else None
                if npc_id:
                    if Npc.objects.filter(id=npc_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id).exists():
                        this_npc = Npc.objects.filter(id=npc_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id)
                        background_description = json.loads(request.POST['background_text'].replace('- d i s p l a y ','').replace('-display',''))
                        background_information = json.loads(request.POST['background_informations'])
                        attributes = json.loads(request.POST['attributes'])
                        skills = json.loads(request.POST['skills'])
                        npc_image = request.POST['npc_image'].split('/')[-1] if not request.POST['npc_image'].split('/')[-1] == 'null' else ''
                        common_items = [{ "name" : key.split('_')[0], "formatted_name" : key.replace(' ', '_').replace("\'","_"), "id" : key.split('_')[1], "quantity": value } for key, value in json.loads(request.POST['common_items']).items() ]
                        template_id = request.POST.get('template_id')
                        name = request.POST.get('npc_name')
                        inventory = json.loads(request.POST['inventory_fields'])
                        inventory_item_name_list = [ value for key, value in inventory.items() ]
                        if ItemInNpc.objects.filter(id_npc_id_id=npc_id, id_game_id_id=game_id[0]).exists():
                            ItemInNpc.objects.filter(id_npc_id_id=npc_id, id_game_id_id=game_id[0]).delete()
                        for picture in inventory_item_name_list:
                            ItemInNpc.objects.create(
                                id_item_id_id = Item.objects.filter(image=picture).values('id'),
                                id_npc_id_id = npc_id,
                                id_game_id_id = game_id[0] 
                            ).save()
                        uuid_full_filename = npc_image
                        if request.FILES:
                            db = GetFromDb(request.user, game_name, "IMAGE")
                            # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                            Document.objects.filter(id_game_id_id=game_id[0], name=npc_image , owner_uuid_id=request.user.id)
                            this_file = FileGestion(request.user.username, game_name, "IMAGE")
                            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(request.FILES['file'].name, "")
                            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                            # create new document for selectionned game.
                            Document.objects.create(
                                name=uuid_full_filename,
                                short_name=no_uuid_shortname,
                                id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                owner_uuid_id=request.user.id,
                                id_game_id_id=game_id,
                                file=this_file.get_full_path()
                            ).save()
                            
                        this_npc.update(
                            name = name ,
                            attributes = attributes,
                            skills = skills,
                            information = background_information,
                            common_item = common_items,
                            description = background_description,
                            inventory = inventory,
                            image=uuid_full_filename
                        )
                        response_data = {'success' : True}
                        return JsonResponse(response_data, safe=False) 
                        
                    else:
                        return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})   
                
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_npc(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                npc_informations = json.loads(request.POST['npc_informations'])
                for key, value in npc_informations.items():
                    if Npc.objects.filter(name=key, id=value, id_game_id_id=game_id[0], owner_uuid_id=request.user.id).exists():
                        image_name = Npc.objects.values_list('image', flat=True).filter(name=key, id=value, id_game_id_id=game_id[0], owner_uuid_id=request.user.id)[0]
                        if not image_name == '':
                            file = FileGestion(request.user.username, game_name, "IMAGE")
                            file.delete_uploaded_file({'image_name':image_name})
                            Document.objects.filter(id_game_id_id=game_id[0], name=image_name, owner_uuid_id=request.user.id).delete()
                        this_npc = Npc.objects.filter(name=key, id=value, id_game_id_id=game_id[0], owner_uuid_id=request.user.id).delete()
                        # other table with this npc will be cleaned too.
                        NpcInChapter.objects.filter(id_game_id_id=game_id[0], id_npc_id_id=value).delete()
                        NpcInScenario.objects.filter(id_game_id_id=game_id[0], id_npc_id_id=value).delete()
                        NpcInScene.objects.filter(id_game_id_id=game_id[0], id_npc_id_id=value).delete()
                        ItemInNpc.objects.filter(id_game_id_id=game_id[0], id_npc_id_id=value).delete()
                    else:
                        return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
                    
                response_data = { 'success': True }
                return JsonResponse(response_data)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
 
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def add_npc_in_scene(request, game_name, pk):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                if Scene.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    npc_id = [ int(npc_id) for npc_id in request.POST.getlist('npc')]
                    for npc in npc_id:
                        if NpcInScene.objects.filter(id_npc_id_id = npc, id_scene_id_id = pk, id_game_id_id = game_id[0]).exists() is False:
                            NpcInScene.objects.create(
                                id_npc_id_id = npc,
                                id_scene_id_id = pk,
                                id_game_id_id=game_id
                            )
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_npc_from_scene(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                scene_id = json.loads(request.POST['scene_id'])
                npc_id = json.loads(request.POST['npc_informations'])
                if Scene.objects.filter(id=int(scene_id['id']), owner_uuid_id=request.user.id).exists():
                    only_npc_id_list = [ int(value) for key, value in npc_id.items()]
                    if NpcInScene.objects.filter(id_npc_id_id__in=only_npc_id_list, id_scene_id_id=int(scene_id['id']), id_game_id_id=game_id[0]).exists():
                        delete_npc = NpcInScene.objects.filter(id_npc_id_id__in=only_npc_id_list, id_scene_id_id=int(scene_id['id']), id_game_id_id=game_id[0])
                        delete_npc.delete()
                        reponse_data = {'success': True}
                    return JsonResponse(reponse_data, safe=False)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

    
@login_required
@ensure_csrf_cookie
@csrf_protect
def get_new_item_informations(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                item_id = int(request.POST.get('id'))
                if Item.objects.filter(id=item_id, id_game_id_id=game_id[0], deleted_at__isnull=True).exists():
                    item_information = Item.objects.filter(id=item_id, id_game_id_id=game_id[0], deleted_at__isnull=True).values('id', 'name', 'formated_name', 'description', 'effect', 'image').first()
                    response = {"success":True, "data":item_information}
                    return JsonResponse(response, safe=False)
                else:
                    response = {"fail":True}
                    return JsonResponse(response, safe=False)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
                                    
  
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_scenario_form(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == "POST":
            name = request.POST['scenario_name']
            if Scenario.objects.filter(name=name, owner_uuid_id=str(request.user.id)).exists():
                scenario_values = Scenario.objects.defer('created_at','updated_at','deleted_at').values('name','description','npc_list').filter(name=name, owner_uuid_id=str(request.user.id))
                response_data = [ key for key in scenario_values if key != "created_at" and key != "updated_at" and key != "deleted_at"]
                return HttpResponse(json.dumps(response_data))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
        
        
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_location_text(request, game_name, pk):
    response_data = {}
    if request.user.is_authenticated:
        if request.method == "POST":
            game_name = request.POST.get('game_name')
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                new_text = request.POST.get('new_text')
                if Location.objects.filter(id_game_id_id=game_id[0], id=pk, owner_uuid_id=request.user.id).exists():
                    this_location = Location.objects.get(id_game_id_id=game_id[0], id=pk, owner_uuid_id=request.user.id)
                    this_location.description = new_text
                    this_location.save()
                return JsonResponse(response_data)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})    
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_item_description_text(request, game_name, pk):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                item_description = request.POST.get('description')
                if Item.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    this_item = Item.objects.get(id=int(pk), owner_uuid_id=request.user.id)
                    this_item.description = item_description
                    this_item.save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_item_view',kwargs={'game_name': game_name, 'pk': int(pk)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_item_effect_text(request, game_name, pk):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                item_effect = request.POST.get('effect')
                if Item.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    this_item = Item.objects.get(id=int(pk), owner_uuid_id=request.user.id)
                    this_item.effect = item_effect
                    this_item.save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_item_view',kwargs={'game_name': game_name, 'pk': int(pk)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

    
@login_required
@ensure_csrf_cookie
@csrf_protect
def add_location_in_scene(request, game_name, pk):
    if request.user.is_authenticated:
        if request.method == "POST":
            print(request.POST)
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                if Scene.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    location_id = [ int(location_id) for location_id in request.POST.getlist('location')]
                    for location in location_id:
                        if LocationInScene.objects.filter(id_location_id_id=location, id_scene_id_id=pk, id_game_id_id=game_id[0]).exists() is False:
                            LocationInScene.objects.create(
                                id_location_id_id = location,
                                id_scene_id_id = pk,
                                id_game_id_id=game_id
                            )
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_scene_view',kwargs={'game_name': game_name, 'pk': int(pk)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

    
@login_required
@ensure_csrf_cookie
@csrf_protect
def add_npc_in_location(request, game_name, pk):
    print(request.POST)
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                if Location.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    location_id = [ int(location_id) for location_id in request.POST.getlist('location')]
                    for location in location_id:
                        if NpcInLocation.objects.filter(id_location_id_id=location, id_npc_id_id=pk, id_game_id_id=game_id[0]).exists() is False:
                            NpcInLocation.objects.create(
                                id_location_id_id = location,
                                id_npc_id_id = pk,
                                id_game_id_id=game_id
                            )
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_npc_view',kwargs={'game_name': game_name, 'pk': int(pk)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def edit_scene_text(request, game_name, pk):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                scene_description = request.POST.get('description')
                if Scene.objects.filter(id=int(pk), owner_uuid_id=request.user.id).exists():
                    this_scene = Scene.objects.get(id=int(pk), owner_uuid_id=request.user.id)
                    this_scene.description = scene_description
                    this_scene.save()
                    return HttpResponseRedirect(reverse('gesmjgenerator:display_selected_scene_view',kwargs={'game_name': game_name, 'pk': int(pk)}))
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_location_from_scene(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            print(request.POST)
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                scene_id = json.loads(request.POST['scene_id'])
                location_id = json.loads(request.POST.get('location_informations'))
                if Scene.objects.filter(id=int(scene_id['id']), owner_uuid_id=request.user.id).exists():
                    only_location_id_list = [ int(value) for key, value in location_id.items()]
                    if LocationInScene.objects.filter(id_location_id_id__in=only_location_id_list, id_scene_id_id=int(scene_id['id']), id_game_id_id=game_id[0]).exists():
                        delete_location = LocationInScene.objects.filter(id_location_id_id__in=only_location_id_list, id_scene_id_id=int(scene_id['id']), id_game_id_id=game_id[0])
                        delete_location.delete()
                    reponse_data = {'success': True}
                    return JsonResponse(reponse_data, safe=False)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})        
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_npc_from_location(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            print(request.POST)
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                npc_id = json.loads(request.POST['npc_id'])
                location_id = json.loads(request.POST.get('location_informations'))
                if Npc.objects.filter(id=int(npc_id['id']), owner_uuid_id=request.user.id).exists():
                    only_location_id_list = [ int(value) for key, value in location_id.items()]
                    if NpcInLocation.objects.filter(id_location_id_id__in=only_location_id_list, id_npc_id_id=int(npc_id['id']), id_game_id_id=game_id[0]).exists():
                        delete_location = NpcInLocation.objects.filter(id_location_id_id__in=only_location_id_list, id_npc_id_id=int(npc_id['id']), id_game_id_id=game_id[0])
                        delete_location.delete()
                    reponse_data = {'success': True}
                    return JsonResponse(reponse_data, safe=False)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})    
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_location(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
                location_id = json.loads(request.POST['location_informations'])
                only_location_id_list = [ int(value) for key, value in location_id.items()]
                if LocationInScene.objects.filter(id_location_id_id__in=only_location_id_list, id_game_id_id=game_id[0]).exists():
                    LocationInScene.objects.filter(id_location_id_id__in=only_location_id_list, id_game_id_id=game_id[0]).delete()
                if Location.objects.filter(id__in=only_location_id_list, owner_uuid_id=request.user.id).exists():
                    filename_list_queryset = Location.objects.filter(id__in=only_location_id_list).values('image','id')
                    get_chapter_filename_list = {}
                    for value in filename_list_queryset:
                        if not value['image'] is None:
                            get_chapter_filename_list.update({value['id']:value['image']})
                    Location.objects.filter(id__in=only_location_id_list, owner_uuid_id=request.user.id).delete()
                    file = FileGestion(request.user.username, game_name, fileType="image")
                    # accept only dic
                    file.delete_uploaded_file(get_chapter_filename_list)
                    response_data = {'success': True}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

                    
      
@login_required
@ensure_csrf_cookie
@csrf_protect
def upload_game_image(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            # get request values in UploadFileForm
            form = UploadImageFileForm(request.POST, request.FILES)
            # check if form is okay
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    # get image_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    image_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    image_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', image_name)
                    # get original_file_filename
                    original_file_filename = request.FILES['file'].name
                    db = GetFromDb(request.user, game_name, "IMAGE")
                    # get game_id
                    game_id = db.get_game_id()
                    if Document.objects.filter(name=image_name, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).exists():
                        return HttpResponseRedirect(reverse('gesmjgenerator:display_gallery_pictures_view_name_already_in_use', kwargs={'game_name': game_name}))
                    else:
                        # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                        this_file = FileGestion(request.user.username, game_name, "IMAGE")
                        uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(request.FILES['file'].name, "")
                        this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                        # create new documentdb.get_pargen_id(uuid_full_filename)
                        if db.get_pargen_id(uuid_full_filename) is False:
                            return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_fail_image_ext', kwargs={'game_name': game_name}))
                        else:
                            # create new document for selectionned game.
                            Document.objects.create(
                                name=uuid_full_filename,
                                short_name=no_uuid_shortname,
                                id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                                owner_uuid_id=request.user.id,
                                id_game_id_id=game_id,
                                file=this_file.get_full_path()
                            ).save()
                            # append keyword table.
                            Keyword.objects.create(
                                id_pargenKeyword_id_id = Pargen.objects.filter(value='vide', deleted_at__isnull=True).values_list('id', flat=True)[0],
                                id_game_id_id = game_id,
                                id_document_id_id = Document.objects.filter(short_name=no_uuid_shortname, owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)[0]
                            ).save()
                            return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_image_view_success', kwargs={'game_name': game_name}))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def upload_game_document(request, game_name):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == 'POST':
            # get request values in UploadFileForm
            form = UploadDocumentFileForm(request.POST, request.FILES)
            # check if form is okay
            if form.is_valid():
                # check if game already exists:
                if not Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                    return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
                else:
                    # get document_name and original file name and replace whitespace.
                    # regexp to rename and delete every not needed symbols
                    # allow using "." and " ' " but not on start or end
                    document_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', request.POST.get('name'))
                    document_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', document_name)
                    # get original_file_filename
                    original_file_filename = request.FILES['file'].name
                    db = GetFromDb(request.user, game_name, "DOC")
                    # set "IMAGE" or "DOC" to looking for any Pargen's id which are "IMAGE" or "DOC" type.
                    this_file = FileGestion(request.user.username, game_name, "DOC")
                    uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(document_name, "")
                    this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                    # pdf thumbnail's part.
                    # set new fitz instance and set path
                    pdf_filename = this_file.get_full_filepath(uuid_full_filename, uuid_filename)
                    # open open
                    doc = fitz.open(pdf_filename)
                    # number of page
                    page = doc.loadPage(0)
                    # get picture.
                    pix = page.getPixmap()
                    # set thumbnail name
                    thumbnail_path = pdf_filename.replace(uuid_full_filename, document_name+'-thumb.png')
                    # writ it in the right directory
                    pix.writePNG(thumbnail_path)
                    # get game_id
                    game_id = db.get_game_id()
                    # create new document
                    if db.get_pargen_id(original_file_filename) is False:
                        return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_document_view_fail_image_ext', kwargs={'game_name': game_name}))
                    else:
                        # create new document for selected game.
                        Document.objects.create(
                            name=uuid_full_filename,
                            short_name = document_name,
                            thumbnail_name = document_name+'-thumb.png',
                            id_pargen_type_id_id=db.get_pargen_id(original_file_filename),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            file=this_file.get_full_path()
                        ).save()
                        return HttpResponseRedirect(reverse('gesmjgenerator:upload_game_document_view_success', kwargs={'game_name': game_name}))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            form = UploadFileForm()
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view', args=[response_data]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def file_name_check(request, game_name, type, target_name, id='0', order='0'):
    """
        check if name already exists in game.
    """
    if request.user.is_authenticated:
        target_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', target_name)
        edited_target_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', target_name)
        if id != '0':
            parent_id = int(id)
        order_error = {}
        response_data={}
        game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
        if type == "scenario" or type == "scene" or type == "chapter" or type == "item" or type == "location" or type == "document" or type == "picture" or type == "npc" :
            
            if type == "picture" or type == "document":
                if type == "picture":
                    response_var = "Une image portant ce nom est déjà liée à ce jeu."
                else:
                    response_var = "Un document portant ce nom est déjà lié à ce jeu."
                if Document.objects.filter(short_name=edited_target_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists():
                    response_data = {'fail_name': response_var}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}  
            
            elif type == "scenario":
                if Scenario.objects.filter(name=edited_target_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists():
                    response_data = {'fail_name': "Un scénario portant ce nom est déjà lié à ce jeu."}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}
            
            elif type == "item":
                if Item.objects.filter(name=edited_target_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists():
                    response_data = {'fail_name': "Un objet portant ce nom est déjà lié à ce jeu."}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}
            
            elif type == "location":
                if Location.objects.filter(name=edited_target_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists():
                    response_data = {'fail_name': "Un lieu portant ce nom est déjà à ce jeu."}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}
        
            elif type == "chapter":
                if Chapter.objects.filter(name=edited_target_name, owner_uuid_id=request.user.id, id_scenario_id_id=parent_id).exists():
                    response_data = {'fail_name': "Un chapitre portant ce nom est déjà lié à ce scenario."}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}
                if order != '0':
                    if Chapter.objects.filter(owner_uuid_id=request.user.id, order=order, id_scenario_id_id=parent_id).exists():
                        order_error = {'fail_order': 'Un chapitre avec cet ordre a déjà été définit.'}
                    else:
                        order_error = {'fail_order': False}
            
            elif type == "scene":
                if Scene.objects.filter(name=edited_target_name, owner_uuid_id=request.user.id, id_chapter_id_id=parent_id).exists():
                    response_data = {'fail_name': "Une scene portant ce nom est déjà liée à ce jeu ou à ce scénario."}
                else:
                    response_data = {'success_name': "ce nom est libre, vous pouvez l'utiliser."}
                if order != '0':
                    if Scene.objects.filter(owner_uuid_id=request.user.id, order=order, id_chapter_id_id=parent_id).exists():
                        order_error = {'fail_order': 'Une scene avec cet ordre a déjà été définit.'}
                    else:
                        order_error = {'fail_order': False}
        data = {"response_data":response_data, "order_error":order_error}
        return JsonResponse(data, safe=False)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def npc_template_name_check(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            response_data = {}
            template_name = request.POST.get('template_name')
            game_name = request.POST.get('game_name')
            if Game.objects.filter(owner_uuid_id=request.user.id, name=game_name, deleted_at__isnull=True).exists():
                game_id = Game.objects.filter(owner_uuid_id=request.user.id, name=game_name, deleted_at__isnull=True).values_list('id', flat=True)
                if NpcTemplate.objects.filter(owner_uuid_id=request.user.id, name=template_name, id_game_id_id=game_id[0]).exists():
                    response_data = {'fail': True}
                    return JsonResponse(response_data)
                else:
                    response_data = {'success': True}
                    return JsonResponse(response_data)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def npc_edit_sheet(request, game_name):
    """
        check element sent and update npc sheet.
    """
    if request.user.is_authenticated:
        response_data={}
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            accepted_key = ['name','image', 'informations', 'background', 'skills', 'attributs', 'items', 'common-items']
            if request.POST.get('data_type') in accepted_key:
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True).first()
                data_type = request.POST.get('data_type')
                npc_id = request.POST.get('npc_id')
                if data_type == "image":
                    old_path = request.POST.get('old_full_path') if request.POST.get('old_full_path') != '' else None
                    if Npc.objects.filter(id=npc_id, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).exists():
                        # set "IMAGE" to search every Pargen's id which are "IMAGE" type.
                        this_file = FileGestion(request.user.username, game_name, data_type)
                        uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(request.FILES['file'].name, "")
                        this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
                        # create new document for selectionned game.
                        db = GetFromDb(request.user, game_name, "IMAGE")
                        Document.objects.create(
                            name=uuid_full_filename,
                            short_name=no_uuid_shortname,
                            id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                            owner_uuid_id=request.user.id,
                            id_game_id_id=game_id,
                            file=this_file.get_full_path()
                        ).save()
                        old_file_id = Document.objects.filter(name=old_path.split('/')[-1], owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True).first()
                        Document.objects.filter(id=old_file_id, owner_uuid_id=request.user.id).delete()
                        Npc.objects.filter(id=npc_id, owner_uuid_id=request.user.id, id_game_id_id=game_id, image=request.POST.get('image_name')).update(image=uuid_full_filename)
                elif data_type == "name":
                    new_name = request.POST.get("""new_name""")
                    old_name = request.POST.get("""old_name""")
                    if Npc.objects.filter(id=npc_id, name=old_name, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).exists():
                        test = Npc.objects.filter(id=npc_id, name=old_name, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).update(name=new_name)
                        response_data = {'success': True, 'new_name': new_name}
                    else:
                        response_data = {'fail' : True }
                    return JsonResponse(response_data, safe=True)
                
                
@login_required
@ensure_csrf_cookie
@csrf_protect
def update_element(request, game_name, old_file_name, new_short_name, element_type,  element_subtype='element_subtype', element_subtype_id='element_subtype_id', pk='document_id', old_thumbnail_name='', keyword=''):
    if request.user.is_authenticated:
        response_data={}
        game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
        if element_subtype_id != 'element_subtype_id':
            element_subtype_id = int(element_subtype_id)
        # regexp to rename and delete every not needed symbols
        # allow using "." and " ' " but not on start or end
        new_file_short_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)', '-', new_short_name)
        new_file_short_name = re.sub(r"(^-+)|(^'+)|(^\.+)|(-$)|('\.$)", '', new_file_short_name)
        document_exists = True if Document.objects.filter(name=old_file_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists() else False
        if element_type == "image" or element_type == "document":
            # get files extensions
            if document_exists is True:
                file_extension = ''.join(pathlib.Path(old_file_name).suffix)
                thumbnail_extension = ''.join(pathlib.Path(old_thumbnail_name).suffix)
            else:
                file_extension = ''.join(pathlib.Path(new_short_name).suffix)
                thumbnail_extension = ''.join(pathlib.Path(new_short_name).suffix)
            
            # set full name
            file_uuid = str(uuid.uuid4())
            file_fullname = new_file_short_name.split('.')[0] + '-{}'.format(file_uuid) + file_extension
            thumbnail_fullname = new_file_short_name.split('.')[0] + '-{}-'.format(file_uuid) + 'thumb' + thumbnail_extension 
            upped_str_element_type = element_type.upper()
            db = GetFromDb(request.user, game_name, upped_str_element_type)
            
            # set new FileGestion instance
            this_file = FileGestion(request.user.username, game_name, element_type)
            # set document name
            uuid_full_filename, uuid_filename, no_uuid_shortname = this_file.set_uuid_on_filename(new_file_short_name, "")
            # upload new document
            this_file.handle_uploaded_file(request.FILES['file'], uuid_full_filename, "")
            # delete old document
            this_file.delete_uploaded_file({'file_name': old_file_name})
            
            # get old document data
            old_document_id = [ entry for entry in Document.objects.filter(name=old_file_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).values_list('id',flat=True) ]
            # if old_document isn't empty
            if old_document_id:
                # update old document database entry
                old_document_to_update = Document.objects.get(pk=old_document_id[0])
                old_document_to_update.name=uuid_full_filename
                old_document_to_update.short_name=no_uuid_shortname
                old_document_to_update.id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename)
                old_document_to_update.id_game_id_id=game_id[0]
                old_document_to_update.file=this_file.get_full_path() 
                old_document_to_update.save()
            else:
                # create new one
                old_document_to_update = Document.objects.create(
                    name=uuid_full_filename,
                    short_name=no_uuid_shortname,
                    id_pargen_type_id_id=db.get_pargen_id(uuid_full_filename),
                    id_game_id_id=game_id[0],
                    file=this_file.get_full_path() 
                ).save()
                
            if element_subtype == "scenario":
                if Scenario.objects.filter(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Scenario.objects.get(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "chapter":
                if Chapter.objects.filter(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Chapter.objects.get(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "scene":
                if Scene.objects.filter(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Scene.objects.get(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "npc":
                if Npc.objects.filter(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Npc.objects.get(id=element_subtype_id, owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "item":
                if Item.objects.filter(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Item.objects.get(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "location":
                if Location.objects.filter(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    new_image_name = Location.objects.get(id=element_subtype_id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_image_name.image = uuid_full_filename
                    new_image_name.save()
                    response_data = {'url_success': True}
                else:
                    response_data = {'url_unknown': True}
            elif element_subtype == "picture":
                pass
                response_data = {'url_unknown': True}
            else:
                response_data = {'url_unknown': True}
            return JsonResponse(response_data)
        else:
            # fail
            response_data = {'url_fail': True}
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
        
            
@login_required
def delete_element(request, game_name, document_name, id, element_type, element_subtype):
    """
        Delete document , and clean database add "empty" to "image" column.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).values_list('id', flat=True)
            if Document.objects.filter(name=document_name, owner_uuid_id=request.user.id, id_game_id_id=game_id[0]).exists():
                if element_type == "image" or element_type == "document" or element_type == "npc" or element_type == "location" or element_type == "item":
                    
                    db_element_type = "document" if element_type == "image" else element_type
                    file_gestion = FileGestion(request.user.username, game_name, db_element_type)
                    document_list_name = {
                        'document_thumb' : document_name.split('.pdf')[0]+('-thumb.png'),
                        'document_name' : document_name 
                    }
                    file_gestion.delete_uploaded_file(document_list_name)
                    in_db = UpdateDb(request.user.id, game_id, game_name)
                    model_tables = {
                        'document':[
                                    'documentinchapter',
                                    'documentinitem',
                                    'documentinlocation',
                                    'documentinnpc',
                                    'documentinscenario',
                                    'documentinscene',
                                    'document'
                                    ],
                        'npc':[
                                    'npc',
                                    'npcinscenario',
                                    'npcinchapter',
                                    'npcinscene',    
                        ],
                        'location':[
                                    'location',
                                    'locationinscenario',
                                    'locationinchapter',
                                    'locationinscene',  
                        ]
                    }
                    table_name = element_subtype.lower()
                    table = apps.get_model(app_label='gestion', model_name=table_name)
                    if(table_name == "document"):
                        image_name = table.objects.get(owner_uuid_id=request.user.id, name=document_name, deleted_at__isnull=True)
                        image_name.delete()
                    else:
                        image_name = table.objects.get(owner_uuid_id=request.user.id, image=document_name, deleted_at__isnull=True)
                        image_name.image = None
                        image_name.save()
                        
                    response_data = {'success' : 'Vous avez supprimé le document "<b>{}</b>" avec succès.'.format(document_name)}
                    return JsonResponse(response_data)
                else:
                    return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_this(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.method == "POST":
            # multiple items .
            if(len(request.POST.getlist('id_array[]')) > 0):
                in_db = UpdateDb(request.user.id, request.session['selected_game_id'], "IMAGE")
                id_list = request.POST.getlist('id_array[]')
                category = request.POST['content_type']
                name_list = request.POST.getlist('name[]')
                model_tables = {
                    'document':['document',
                                'documentinchapter',
                                'documentinitem',
                                'documentinlocation',
                                'documentinnpc',
                                'documentinscenario',
                                'documentinscene'],
                    'npc':['npc',
                        'npcinscenario',
                        'npcinchapter',
                        'npcinscene',    
                    ],
                    'location':['location',
                        'locationinscenario',
                        'locationinchapter',
                        'locationinscene',  
                    ]
                }
                
                file_gestion = FileGestion(request.user.username, request.session['selected_game'])
                for table_name, table_fields in model_tables.items():
                    if len(id_list) == 1:
                        in_db.delete_from_db(id_list, table_fields)
                    elif len(id_list) > 1:
                        in_db.delete_from_db(id_list, table_fields)         
                if category == "document":
                    file_gestion.delete_uploaded_file(name_list)
                elif category == "npc":
                    response_data.update({'category': category, 'id_list': id_list})
                return JsonResponse(response_data)
                        
        return HttpResponseRedirect(reverse('gesmjgenerator:dashboard'))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

@login_required
@ensure_csrf_cookie
@csrf_protect
def delete_game(request, game_name):
    """
        Delete game , and clean database. 
    """
    if request.user.is_authenticated:
        response_data = {}
        owner_id = request.user.id
        if Game.objects.filter(name=game_name, owner_uuid_id=owner_id, deleted_at__isnull=True).exists():
            game_id = Game.objects.filter(name=game_name, owner_uuid_id=owner_id).values_list('id', flat=True)
            in_db = UpdateDb(request.user.id, game_id[0])
            # set tables which will be targeted for deletion
            model_tables = {
                'document':['document',
                            'documentinchapter',
                            'documentinitem',
                            'documentinlocation',
                            'documentinnpc',
                            'documentinscenario',
                            'documentinscene'],
                'npc':['npc',
                       'npcinscenario',
                       'npcinchapter',
                       'npcinscene',    
                ],
                'location':['location',
                       'locationinscenario',
                       'locationinchapter',
                       'locationinscene',  
                ]
            }
            for table_name, table_fields in model_tables.items():
                data = {'game_id': game_id[0]}
                in_db.delete_from_db(data, table_fields)
                
            # prepare others table
            file_gestion = FileGestion(request.user.username, game_name, "IMAGE")
            scenarios_id = Scenario.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).values_list('id', flat=True)
            chapters_id = Chapter.objects.filter(id_scenario_id_id__in=scenarios_id, owner_uuid_id=owner_id).values_list('id', flat=True)
            scenes_id = Scene.objects.filter(id_chapter_id__in=chapters_id, owner_uuid_id=owner_id).values_list('id', flat=True)
            
            # delete content
            Scene.objects.filter(id__in=scenes_id, owner_uuid_id=owner_id).delete()
            Chapter.objects.filter(id__in=chapters_id, owner_uuid_id=owner_id).delete()
            Scenario.objects.filter(id__in=scenarios_id, owner_uuid_id=owner_id).delete()
            Note.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            DetailedNote.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            Npc.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            NpcTemplate.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            Item.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            Location.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=owner_id).delete()
            Keyword.objects.filter(id_game_id_id=game_id[0]).delete()
            Game.objects.filter(id=game_id[0], owner_uuid_id=owner_id).delete()
            
            # delete game file
            file_gestion.clean_in_game_directory()
            return HttpResponseRedirect(reverse('gesmjgenerator:delete_game_view_success'))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def file_color_change(request, game_name, file_type, id, color):
    """
        change color value inside db table. 
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            color = "#"+color
            if file_type == "scenario":
                if Scenario.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Scenario.objects.get(id=id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            if file_type == "chapter":
                if Chapter.objects.filter(owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Chapter.objects.get(id=id, owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            if file_type == "scene":
                if Scene.objects.filter(owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Scene.objects.get(id=id, owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            if file_type == "item":
                if Item.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Item.objects.get(id=id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            if file_type == "npc":
                if Npc.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Npc.objects.get(id=id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            if file_type == "location":
                if Location.objects.filter(id_game_id_id=game_id[0], owner_uuid_id=request.user.id, id=id, deleted_at__isnull=True).exists():
                    new_color_code = Location.objects.get(id=id, id_game_id_id=game_id[0], owner_uuid_id=request.user.id, deleted_at__isnull=True)
                    new_color_code.color = color
                    new_color_code.save()
                    response_data = {'success': True}
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
                
@login_required
@ensure_csrf_cookie
@csrf_protect
def save_note(request):
    """
        save note in db.
    """
    if request.user.is_authenticated:
        game_id = int(request.POST.get('game_id'))
        note = str(request.POST.get('note')) if str(request.POST.get('note')) != '' else None
        if Game.objects.filter(id=game_id, owner_uuid_id=request.user.id).exists():
            obj, created = Note.objects.update_or_create(
                owner_uuid_id=request.user.id, id_game_id_id=game_id, 
                defaults={'note':note},
            )
            response_data = {'success': True}
            return JsonResponse(response_data)
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

@login_required
@ensure_csrf_cookie
@csrf_protect
def create_detailed_note(request, game_name):
    """
    :param request:
    :param game_name
    :change stylesheet db value and return to homepage.
    """
    if request.user.is_authenticated:
        if request.method == "POST":
            if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
                game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
                formatted_name = re.sub(r'([\$\!\#\_\&\"\£\*\%\?\^\ \=\[\]\{\}\(\)\:\=\<\>\;\§\%\~\|\_\^\@][\W]*)','_', request.POST.get('name'))
                formatted_name = re.sub(r"(^-+)|(^_+)|(^'+)|(^\.+)|(-$)|(_$)|('\.$)", '', formatted_name)+'_'+str(random.randint(1,100000))
                DetailedNote.objects.create(
                    name = request.POST.get('name') if request.POST.get('name') != '' else 'Sans titre',
                    formatted_name = formatted_name,
                    note = request.POST.get('note') if request.POST.get('note') != '' else '',
                    owner_uuid_id = request.user.id,
                    id_game_id_id = game_id
                ).save()
                return HttpResponseRedirect(reverse('gesmjgenerator:note_creation_success_view', kwargs={'game_name': game_name}))
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
  
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def save_note_content(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            formatted_name = request.POST.get('formatted_name')
            note_id = int(request.POST.get('note_id'))
            note_content = request.POST.get('text_content')        
            response_data = ''
            game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            if DetailedNote.objects.filter(formatted_name=formatted_name, id=note_id, owner_uuid_id=request.user.id, id_game_id_id=game_id[0], deleted_at__isnull=True).exists():
                edited_note = DetailedNote.objects.get(formatted_name=formatted_name, id=note_id, owner_uuid_id=request.user.id, id_game_id_id=game_id[0], deleted_at__isnull=True)
                edited_note.note = note_content
                edited_note.save()
                response_data = {'success':True}
            else:
                response_data = {'success':False}
            return JsonResponse(response_data)
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
        
@login_required
@ensure_csrf_cookie
@csrf_protect
def suppress_note_content(request, game_name):
    if request.user.is_authenticated:
        if request.method == "POST":
            note_id = int(request.POST.get('note_id'))
            formatted_name = request.POST.get('formatted_name')
            response_data = ''
            game_id = Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            if DetailedNote.objects.filter(formatted_name=formatted_name, id=note_id, owner_uuid_id=request.user.id, id_game_id_id=game_id[0], deleted_at__isnull=True).exists():
                DetailedNote.objects.filter(id=note_id).delete()
                response_data = {'success':True}
            else:
                response_data = {'success':False}
            return JsonResponse(response_data)
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            
                
@login_required
@ensure_csrf_cookie
@csrf_protect
def select_theme(request):
    """
    :param request:
    :param none
    :change stylesheet db value and return to homepage.
    """
    if request.user.is_authenticated:
        if request.method == "POST":
            last_url = request.POST.get('next', '/')
            theme = request.POST.get('theme') if request.POST.get('theme') != '' else None
            css_stylesheet_directory = settings.STATICFILES_DIRS[0]  + '/css/'
            stylesheet_dir_content = os.listdir(css_stylesheet_directory)
            directory = FileGestion(request.user.username).get_directory_content(css_stylesheet_directory +'*.css' )
            if theme in directory:
                User.objects.filter(id=request.user.id).update(
                    user_stylesheet=theme
                )
                return HttpResponseRedirect(last_url)
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:dashboard_stylesheet_unknown_view'))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
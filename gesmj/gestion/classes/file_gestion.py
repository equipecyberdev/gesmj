# -*- coding: utf-8 -*-

import glob
import pathlib

from gestion.models import *
from .get_from_db import GetFromDb


class FileGestion:
    """
        Class which is used to create from after user's creation success
    """
    def __init__(self, username, gameName="none", fileType="none"):
        self.username = username
        self.fileType = fileType.upper()
        self.db = GetFromDb(username, None,  self.fileType)
        self.path = settings.MEDIA_ROOT
        self.gameName = gameName
        self.fullpath = os.path.join(self.path, str(self.db.get_user_id()), self.gameName)
            
    def get_full_path(self):
        return self.fullpath

    def __check_and_create_user_directory(self):
        """
             Check if directory exist or not, if not create it.
            :return: nothing.
        """
        if os.path.exists(self.fullpath) is False:
            os.makedirs(self.fullpath)
    
    def set_uuid_on_filename(self, uploaded_filename, filename = ""):
        # get file extension
        if self.fileType == "DOC":
            extension = ".pdf" 
        else:
            extension = '.' + uploaded_filename.split('.')[-1]
        # get filename without extension
        filename_without_uuid = uploaded_filename.split('.')[0:-1]
        # create uuid variable
        filename_uuid = "{}".format(str(uuid.uuid4()))
        uploaded_filename_with_uuid = "{}".format(filename_uuid + extension)
        # from list to str 
        if filename != "":
            filename += '-' + filename_without_uuid + extension
        # from list to str   
        shortname = "".join(filename_without_uuid)
        print(extension, uploaded_filename_with_uuid, shortname)
        return uploaded_filename_with_uuid, filename, shortname
   
    def handle_uploaded_file(self, file, uploaded_filename, filename="") :
        """
            :param file
            :param uploaded_filename
            :param filename
            check extension, set extension if none.
            write new file (with byte append option) in user's folder.
        """
        self.__check_and_create_user_directory()
        
        full_filename_with_extension = self.db.get_filename_with_extension(uploaded_filename, filename)
        full_filepath = os.path.join(self.fullpath, full_filename_with_extension)
        with open(full_filepath, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
                
    def get_file_fullname_with_extension(self, uploaded_filename, old_filename):
        return self.db.get_filename_with_extension(uploaded_filename, old_filename)
                
    def rename_file(self, filename, new_short_name):
        uploaded_filename_with_ext = new_short_name + "." + pathlib.Path(filename).suffix
        return os.rename(os.path.join(os.path.join(self.fullpath, filename)), os.path.join(self.fullpath, uploaded_filename_with_ext))
                
    def rename_file_no_suffix(self, filename, new_short_name):
        return os.rename(os.path.join(os.path.join(self.fullpath, filename)), os.path.join(self.fullpath, new_short_name))
    
    def get_renamed_file(self, filename, new_short_name):
        return new_short_name + "." + pathlib.Path(filename).suffix
    
    def get_full_filepath(self, uploaded_filename, filename):
        full_filename_with_extension = self.db.get_filename_with_extension(uploaded_filename, filename)
        return os.path.join(self.fullpath, full_filename_with_extension)
    
    def rename_game_directory(self, new_game_name):
        return os.rename(self.fullpath, os.path.join(self.path, str(self.db.get_user_id()), new_game_name))
    
    def delete_uploaded_file(self,name_list={}):
        """
            :param game_id
            :param id (of what you want to delete)
            get filename and delete it
        """
        for key, value in name_list.items():
            full_file_path = self.fullpath + '\\' + value
            if os.path.exists(full_file_path):
                os.remove(full_file_path)
        
    def clean_in_game_directory(self):
        """
            :param none
            delete every file inside a game directory and game directory
            Used for game deleting.
        """
        # define which directory we need to clean
        # and store filename in list
        files = glob.glob(self.fullpath + '\\*')
        # deleting all file inside
        for file in files:
            os.remove(file)
        # cleaning directory
        os.rmdir(self.fullpath)
    
    def get_directory_content(self, directory_path):
        """
            :param directory_name
            delete every file inside a game directory and game directory
            Used for game deleting.
        """
        return [os.path.basename(x) for x in glob.glob(directory_path)]
        



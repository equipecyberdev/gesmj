from django.contrib.sessions.backends.db import SessionStore, SessionBase
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.contrib.auth import logout
from django.http import request
from .get_from_db import GetFromDb
import json


class UserSession:
    """
        Store user data in session
    """
    def __init__(self, username):
        self.username = username
        self.date_now = timezone.now()
        self.user_db = GetFromDb(self.username)
        self.user_informations, self.user_game = self.user_db.get_all_data()
        self.user_session = SessionStore()


    def __set_session(self):
        """
            store all data from __get_all_user_data in session.
        """
        if not self.user_session.get('username', self.user_informations['username']):
            self.user_session.create()
        self.user_session['id'] = self.user_informations['id']
        self.user_session['username'] = self.user_informations['username']
        self.user_session['last_login'] = json.dumps(self.user_informations['last_login'], indent=4, sort_keys=True, default=str)
        self.user_session['email'] = self.user_informations['email']
        self.user_session['is_active'] = self.user_informations['is_active']
        self.user_session['date_joined'] = json.dumps(self.user_informations['date_joined'], indent=4, sort_keys=True, default=str)
        self.user_session['first_name'] = self.user_informations['first_name']
        self.user_session['last_name'] = self.user_informations['last_name']
        self.user_session['is_staff'] = self.user_informations['is_staff']
        self.user_session['is_superuser'] = self.user_informations['is_superuser']
        self.user_session['game'] = [value['name'] for value in self.user_game.values()]

        
    def set_in_session(self, key, value):
        """
            store new data
        """
        if not self.user_session.get('username', self.user_informations['username']):
            self.user_session.create()
        self.user_session[str(key)] = value


    def get_session_txt(self):
        return self.user_session


    def __get_session_expire_date(self):
        """
            return session expire_date
        """
        return self.user_session.get_expiry_date


    def delete_session(self):
        """
            use logout(request) method and delete session using session_key as filter
        """
        logout(request)
        self.user_session.objects.filter(session_key=self.user_session.session_key).delete()
        
    def delete(self, session_key=None):
        if session_key is None:
            if self.session_key is None:
                return
            session_key = self.session_key
        try:
            Session.objects.get(session_key=session_key).delete()
        except Session.DoesNotExist:
            pass


    def session_exist(self):
        """
            return true if self.user_session is not empty
        """
        if self.user_session is not None:
            return True

    def set_session(self):
        return self.__set_session()

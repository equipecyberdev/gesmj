# -*- coding: utf-8 -*-

import pathlib

from django.apps import apps
from django.core import serializers
from django.utils import timezone

from ..models import *


class GetFromDb:
    """
        Get informations from db.
    """
    def __init__(self, username, selected_game=None, fileType=None):
        self.username = username
        self.date_now = timezone.now()
        # 'IMAGE' or 'DOC'
        self.fileType = fileType
        self.selected_game = selected_game


    def __get_user_id(self):
        """
            return user id
        """
        return User.objects.only('id').get(username=self.username).id
    
    
    def get_keyword_id(self, keyword_value):
        """
            return pargen keyword list
        """
        return Pargen.objects.filter(pargen_type="KEYWORD", value=keyword_value, deleted_at__isnull=True).values_list('id', flat=True)[0]
    
    def get_game_list(self):
        """
            return user game list
        """
        return Game.objects.filter(owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).values()
    
    
    def get_game_id(self):
        """
            return game id
        """
        return Game.objects.only('id').get(name=self.selected_game, owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).id
    
    
    def __get_scenarios_id(self):
        """
            return user scenarios id
        """
        scenarios = Scenario.objects.only('id').filter(id_game_id_id=self.get_game_id(), deleted_at__isnull=True).values()
        scenarios_id = [value['id'] for value in scenarios.values()]
        return scenarios_id
    
    
    def __get_chapters_id(self):
        """
            return user scenarios id
        """
        chapters = Chapter.objects.only('id').filter(id_scenario_id_id__in=self.__get_scenarios_id(), deleted_at__isnull=True).values()
        chapters_id = [value['id'] for value in chapters.values()]
        return chapters_id
    
    
    def __get_scenes_id(self):
        """
            return user scenarios id
        """
        scenes = Scene.objects.only('id').filter(id_chapter_id_id__in=self.__get_chapter_id(), deleted_at__isnull=True).values()
        scenes_id = [value['id'] for value in scenes.values()]
        return scenes_id
    
    
    def __get_documents_id(self):
        """
            return user scenarios id
        """
        document = Scene.objects.only('id').filter(owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).values()
        document_id = [value['id'] for value in document.values()]
        return document_id
        
    
    def check_game_exists(self):
        return Game.objects.filter(owner_uuid_id=self.__get_user_id(), name=self.selected_game, deleted_at__isnull=True).exists()
    
    
    def get_game_scenarios(self):
        """
            return user's scenario dict
        """
        return Scenario.objects.defer('created_at', 'deleted_at', 'updated_at').filter(id_game_id_id=self.get_game_id(), deleted_at__isnull=True).values()
        
    
    def get_scenarios_chapters(self):
        """
            return user's scenario dict
            __in suffix to use list
        """
        return Chapter.objects.defer('created_at', 'deleted_at', 'updated_at').filter(owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).values()
    

    def get_chapters_scenes(self):
        """
            return user's scenario
        """
        return Scene.objects.defer('created_at', 'deleted_at', 'updated_at').filter(owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).values()
    
    
    def get_documents(self):
        """
            return game's documents
        """
        return Document.objects.defer('deleted_at').filter(owner_uuid_id=self.__get_user_id(), id_game_id_id=self.get_game_id(), deleted_at__isnull=True).values()
    
    
    def get_npcs(self):
        """
            return game's npc
        """
        return Npc.objects.defer('created_at', 'deleted_at', 'updated_at').filter(owner_uuid_id=self.__get_user_id(), id_game_id_id=self.get_game_id(), deleted_at__isnull=True).values()
    
    
    def get_locations_game(self):
        """
            return user's ingame location
        """
        return Location.objects.defer('created_at','deleted_at','updated_at').filter(owner_uuid_id=self.__get_user_id(), id_game_id_id=self.get_game_id(), deleted_at__isnull=True).values()
    
    def get_all_data(self):
        """
            :return: every data from user table
        """

        user_informations = User.objects.get(id=self.__get_user_id(), deleted_at__isnull=True)
        user_informations_array = dict()

        user_game = Game.objects.defer('created_at', 'deleted_at', 'updated_at').only('name').filter(owner_uuid_id=self.__get_user_id(), deleted_at__isnull=True).values()
        user_game_array = dict()

        for it_user in range(0, 1):
            user_informations_array.update({
                "id": self.__get_user_id(),
                "username": user_informations.username,
                "last_login": user_informations.last_login,
                "email": user_informations.email,
                "is_active": user_informations.is_active,
                "date_joined": user_informations.date_joined,
                "first_name": user_informations.first_name,
                "last_name": user_informations.last_name,
                "is_staff": user_informations.is_staff,
                "is_superuser": user_informations.is_superuser,
            })

        for it_user in range(0, len(user_game)):
            user_game_array.update({
                it_user: {
                    "name": user_game[it_user]['name'],
                }
            })
        return user_informations_array, user_game_array
        
    
        
    def search_in_db_engine(self,data, model_name):
        """
            method which search in db and return result. 
            use "contain" option
        """ 
        if model_name != None: 
            model_name = model_name.lower()
            # get model name, store it in "model" variable
            model = apps.get_model('gestion', model_name)
            # set criteria used to communicate with db.
            model_criteria = {
                'id_game_id_id' : self.selected_game,
                'owner_uuid_id' : self.username,
                'name__contains' : data
            }
            if model.objects.filter(**model_criteria).count() > 0 : 
                return serializers.serialize("json", model.objects.filter(**model_criteria), fields = ("name", "id"))
            else:
                return None
        else:
            return None


    def get_user_id(self):
        return str(self.__get_user_id())

    
    def get_filename_using_id(self, id):
        """
            return filename as a string
        """
        return Document.objects.filter(owner_uuid_id=self.__get_user_id(), id_game_id_id=self.get_game_id(), id=id, deleted_at__isnull=True).only('name').get()


    def __check_if_extension_exists(self, filename):
        """
            return True if extension is finded
        """
        return ''.join(pathlib.Path(filename).suffixes) != ''
            
    def check_if_file_exists(self, filename):
        """
            return True if file exists in db
        """
        return Document.objects.filter(name=filename, id_game_id_id=self.get_game_id(), owner_uuid_id=self.get_game_id(), deleted_at__isnull=True).exists()
            
    
    def get_file_extension(self, uploaded_filename):
        """
            return File's extension
        """
        return ''.join(pathlib.Path(uploaded_filename).suffix)
    
    
    def get_filename_with_extension(self, uploaded_filename, filename=''):
        """
            return filename with extension (taken from uploaded_filename) if filename extension's not set
        """
        if filename != '':
            if self.__check_if_extension_exists(filename):
                return filename + self.get_file_extension(uploaded_filename)
            else:
                return filename + self.get_file_extension(uploaded_filename)
        else:
            return uploaded_filename
    
    
    def __is_allowed_extension(self, uploaded_filename):
        """
            Return True or False if file's extension is allowed or not.
        """
        # take 'IMAGE' or 'DOC' args (fileType)
        pargen_type_list = self.fileType
        # Get file extension
        file_extension = self.get_file_extension(uploaded_filename)
        # Check if this extension exist, if yes return True, else False
        if Pargen.objects.filter(pargen_type=pargen_type_list, value=file_extension, deleted_at__isnull=True).values_list('value', flat=True).exists():
            return True
        else:
            return False
    
    
    def get_pargen_id(self, uploaded_filename):
        """
            Return id_pargen_id or false.
        """
        if self.__is_allowed_extension(uploaded_filename):
            # get extension's ID from Pargent table
            for pargen_id in Pargen.objects.only('id').filter(value=self.get_file_extension(uploaded_filename), deleted_at__isnull=True).values():
                return pargen_id['id']        
        else:
            return False
        
        
    def get_pargen_type(self, pargen_id):
        """
            return pargen_type
        """
        return Pargen.objects.only('pargen_type').filter(id_pargen_type_id_id=pargen_id, deleted_at__isnull=True).values();
        




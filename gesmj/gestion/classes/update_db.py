# -*- coding: utf-8 -*-

from django.apps import apps
from django.conf import settings

from ..models import *


class UpdateDb:
    def __init__(self, user_id, game_id=0, gameName="none"):
        self.user_id = str(user_id)
        self.game_id = game_id
        self.path = settings.MEDIA_ROOT
        self.gameName = gameName

    def add_new_game(self, name):
        """
        :param name:
        :return: nothing
        method which create new game.
        """
        register_new_game = Game.objects.create(
            name=name,
            owner_uuid_id=self.user_id
        )
        register_new_game.save()

    def add_new_scenario(self, name, description, npc_list):
        """
        :param name:
        :param description:
        :param npc_list:
        :param id_game_id_id:
        :return: nothing
        method which create new scenario , and all related data.
        """
        try:
            register_new_scenario = Scenario.objects.create(
                name=name,
                npc_list=npc_list,
                description=description,
                id_game_id_id=self.game_id,
                owner_uuid_id=self.user_id
            )
            register_new_scenario.save()
        except Scenario.DoesNotExist as error:
            return error
        

    def add_new_document(self, name, pargen_id, uuid, game_id, path):
        """
        :param name:
        :param pargen_id
        :param uuid
        :param path
        :param game_id
        method wich create new document.
        """
        try:
            register_new_document = Document.objects.create(
                name = name,
                id_pargen_type_id_id = pargen_id,
                owner_uuid_id = uuid,
                id_game_id_id = game_id,
                file = path
            ).save()
        except Document.DoesNotExist as error:
            return error
            
                
        
    def delete_from_db(self, data, model_list):
        """
            method which delete something in the db;
            can take list argument.
        """
        if len(model_list) > 0:
            for model_name in model_list:
                # get model name, store it in "model" variable
                model = apps.get_model('gestion', model_name)
                # set criteria used to communicate with db.
                model_criteria = {}
                # get 'model_name's fields 
                model_fields =  [field.name for field in model._meta.get_fields()]
                # if len == 1 it's a game, else it's something else.
                if len(data) > 1:
                    if 'id_{}_id'.format(data['category']) in model_fields and 'id_{}_id'.format(data['element_type']) in model_fields:
                        if 'id_game_id' in model_fields:
                            if 'owner_uuid_id' in model_fields:
                                model_criteria = {
                                    'id_{}_id_id'.format(data['category']): data['category_item_id'], 
                                    'id_{}_id_id'.format(data['element_type']) : data['document_id'], 
                                    'id_game_id_id' : data['game_id'], 
                                    'owner_uuid_id': self.user_id
                                }
                            else:
                                model_criteria = {
                                    'id_{}_id_id'.format(data['category']): data['category_item_id'], 
                                    'id_{}_id_id'.format(data['element_type']) : data['document_id'], 
                                    'id_game_id_id' : data['game_id'], 
                                }
                        else:
                            if 'owner_uuid_id' in model_fields:
                                model_criteria = {
                                    'id_{}_id_id'.format(data['category']): data['category_item_id'], 
                                    'id_{}_id_id'.format(data['element_type']) : data['document_id'],  
                                    'owner_uuid_id': self.user_id
                                }
                            else:
                                model_criteria = {
                                    'id_{}_id_id'.format(data['category']): data['category_item_id'], 
                                    'id_{}_id_id'.format(data['element_type']) : data['document_id'],  
                                }
                        model.objects.filter(**model_criteria).delete()
                    else:
                        pass
                else:
                    model_criteria = {'id':data['game_id']}
                    model.objects.filter(**model_criteria).delete()
        else:
            return False
        
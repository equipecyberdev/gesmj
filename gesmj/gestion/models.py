import uuid

from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_jsonfield_backport.models import JSONField

Logged_in_user = settings.AUTH_USER_MODEL

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gesmj.settings")

def get_upload_path(instance, filename):
    return os.path.join('file', str(instance.id), filename)


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    User.username = models.CharField(max_length=16, unique=True)
    USERNAME_FIELD = 'username'
    user_stylesheet = models.CharField(max_length=60, default="stylesheet_default_color.css", null=False)
    
    
# Parametre Generaux
class Pargen(models.Model):
    pargen_type = models.CharField(max_length=10)
    value = models.CharField(max_length=50)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __int__(self):
        return self.id
    
    def __str__(self):
        return self.value
    
    class Meta:
        verbose_name = _("Paramètre général")
        verbose_name_plural = _("Paramètres généraux")


class Game(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=20)
    file = models.FileField(upload_to=get_upload_path, blank=True, null=True) 
    color = models.CharField(max_length=8, default="#ffffff")
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Jeu")
        verbose_name_plural = _("Jeux")


class Scenario(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=30)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    description = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    color = models.CharField(max_length=8, default="#ffffff")
    image = models.CharField(max_length=250, blank=True, null=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Scénario")
        verbose_name_plural = _("Scénarios")



        
class NpcTemplate(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    id_game_id = models.ForeignKey(Game, null=True, on_delete=models.CASCADE)
    statistics = JSONField()
    background = JSONField()
    style = JSONField()
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    
    def __int__(self):
        return self.id

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Template pour PNJ")
        verbose_name_plural = _("Templates pour PNJ")
        
        
class Npc(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_template_id = models.ForeignKey(NpcTemplate, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    attributes = JSONField(null=True, blank=True)
    skills = JSONField(null=True, blank=True)
    information = JSONField(null=True, blank=True)
    description = JSONField(null=True, blank=True)
    inventory = JSONField(null=True, blank=True)
    common_item = JSONField(null=True, blank=True)
    image = models.CharField(max_length=250, blank=True, null=False, default='')
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Pnj")
        verbose_name_plural = _("Pnjs")


class Item(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=45)
    formated_name = models.CharField(max_length=45, blank=True, null=True)
    description = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    effect = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    color = models.CharField(max_length=8, default="#ffffff")
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Objet")
        verbose_name_plural = _("Objets")


class Location(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=45)
    description = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    color = models.CharField(max_length=8, default="#ffffff")
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Lieu")
        verbose_name_plural = _("Lieux")


class Chapter(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, null=True, on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(null=False, default=1)
    name = models.CharField(max_length=45)
    description = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    image = models.CharField(max_length=250, blank=True, null=True)
    document = models.CharField(max_length=250, blank=True, null=True)
    resume = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    note = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    content = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    color = models.CharField(max_length=8, default="#ffffff")
    id_scenario_id = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Chapitre")
        verbose_name_plural = _("Chapitres")


class Scene(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, null=True, on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(null=False, default=1)
    name = models.CharField(max_length=45)
    description = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    image = models.CharField(max_length=150, blank=True, null=True)
    content = RichTextField(config_name='ckeditor_config', blank=True, null=True)
    color = models.CharField(max_length=8, default="#ffffff")
    id_chapter_id = models.ForeignKey(Chapter, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Scène")
        verbose_name_plural = _("Scènes")


# Check if extension exist, if yes :
# To path /media/{{ owner_uuid }}/{{ game_name }}/{{ name  }}
class Document(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=1200)
    short_name = models.CharField(max_length=1200, blank=True, null=True)
    file = models.FileField(upload_to=get_upload_path, null=True, validators=[FileExtensionValidator(allowed_extensions=['pdf','jpg','png'])])
    thumbnail_name = models.CharField(max_length=1200, blank=True, null=True)
    id_pargen_type_id = models.ForeignKey(Pargen, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    
    def __int__(self):
        return self.id

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")



class DocumentInChapter(models.Model):
    id_chapter_id = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class DocumentInNpc(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class DocumentInItem(models.Model):
    id_item_id = models.ForeignKey(Item, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class DocumentInLocation(models.Model):
    id_location_id = models.ForeignKey(Location, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class DocumentInScene(models.Model):
    id_scene_id = models.ForeignKey(Scene, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class DocumentInScenario(models.Model):
    id_scenario_id = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class NpcInChapter(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_chapter_id = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class NpcInScene(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_scene_id = models.ForeignKey(Scene, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class NpcInScenario(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_scenario_id = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class NpcInLocation(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_location_id = models.ForeignKey(Location, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class LocationInChapter(models.Model):
    id_location_id = models.ForeignKey(Location, on_delete=models.CASCADE)
    id_chapter_id = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class LocationInScene(models.Model):
    id_location_id = models.ForeignKey(Location, on_delete=models.CASCADE)
    id_scene_id = models.ForeignKey(Scene, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class LocationInScenario(models.Model):
    id_location_id = models.ForeignKey(Location, on_delete=models.CASCADE)
    id_scenario_id = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class ItemInChapter(models.Model):
    id_item_id = models.ForeignKey(Item, on_delete=models.CASCADE)
    id_chapter_id = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class ItemInScene(models.Model):
    id_item_id = models.ForeignKey(Item, on_delete=models.CASCADE)
    id_scene_id = models.ForeignKey(Scene, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class ItemInScenario(models.Model):
    id_item_id = models.ForeignKey(Item, on_delete=models.CASCADE)
    id_scenario_id = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    
class ItemInNpc(models.Model):
    id_npc_id = models.ForeignKey(Npc, on_delete=models.CASCADE)
    id_item_id = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=False, default=1)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class Keyword(models.Model):
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    id_document_id = models.ForeignKey(Document, on_delete=models.CASCADE)
    id_pargenKeyword_id_id = models.PositiveSmallIntegerField(null=False, default=0)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    def __str__(self):
        return self.id_pargenKeyword_id

    class Meta:
        verbose_name = _("Mot clé")
        verbose_name_plural = _("Mots clés")
        
class Note(models.Model):
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    note = models.CharField(max_length=1200, null=True, blank=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    class Meta:
        verbose_name = _("Note")
        verbose_name_plural = _("Notes")

class DetailedNote(models.Model):
    name = models.CharField(max_length=50, null=True)
    formatted_name = models.CharField(max_length=2500, null=True)
    owner_uuid = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    note = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField('update date', auto_now=True)
    deleted_at = models.DateTimeField('deletion date', null=True)

    class Meta:
        verbose_name = _("Note détaillée")
        verbose_name_plural = _("Notes détaillées")
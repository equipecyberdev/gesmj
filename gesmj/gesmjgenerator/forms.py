import re

from ckeditor.fields import RichTextFormField
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.http import HttpRequest as request
from django.utils.translation import gettext_lazy as _

from gestion.models import *


# VALIDATORS :
def validate_username(value):
    """
        validator which check if username is already in use.
    """
    if value is None:
        raise ValidationError(
            _('Champ obligatoire'),
            params={'value': ''}
        )
    elif User.objects.filter(username=value).exists():
        raise ValidationError(
            _("Le nom d\'utilisateur déjà utilisé").format(value),
            params={'value': value}
        )
    elif len(value) > 16:
        raise ValidationError(
            _("Le nom d'utilisateur est trop grand, 16 caractères maximum.").format(value),
            params={'value': value}
        )
    elif len(value) < 4:
        raise ValidationError(
            _("Le nom d'utilisateur est trop court, 4 caractères minimum.").format(value),
            params={'value': value}
        )
        
        
def validate_email(value):
    """
        validator which check if email input is okay.
    """
    if value is None:
        raise ValidationError(
            _("Champ obligatoire"),
            params={'value': ''}
        )
    elif User.objects.filter(email=value).exists():
        raise ValidationError(
            _("Cette adresse est déjà utilisée").format(value),
            params={'value': value}
        )
    elif not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", value):
        raise ValidationError(
            _("Veuillez entrer une adresse email dans un format correct.")
        )


def validate_password(value):
    if not re.match("^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*_.,?!;#?&])[A-Za-z\d@$!_.,;?!#%*?&]{8,150}$", value):
        raise ValidationError(
            _("Votre mot de passe doit contenir entre 8 et 150 caractères. Au moins un caractère spécial: '$', '@', '#', '%', '_', '?', '!', ';', ',' '.' un minimum de une majuscule et enfin au moins un chiffre"))

# FORMS:
class UserCreateForm(UserCreationForm):   
    username = forms.CharField(
        validators=[validate_username],
        min_length=4, 
        max_length=16, 
        required=True,
        label="Nom d'utilisateur: ",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
    email = forms.EmailField(
        validators=[validate_email],
        required=True,
        label="Adresse email: ",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center',
        }))
    first_name = forms.CharField(
        max_length=50, 
        required=False,
        label="Nom: ",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
    last_name = forms.CharField(
        max_length=150, 
        required=False,
        label="Prenom: ",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center',
        }))
    password1 = forms.CharField(
        validators=[validate_password],
        max_length=150,
        min_length=8, 
        required=True,
        label="Mot de passe: ",
        widget=forms.PasswordInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
    password2 = forms.CharField(
        max_length=150, 
        min_length=8, 
        required=True,
        label="Confirmation: ",
        widget=forms.PasswordInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
 
    class Meta:
        # change to custom user model .
        model = get_user_model()
        fields = ['username', 'email', 'password1', 'password2', 'first_name', 'last_name']


class LoginForm(forms.Form):
    username = forms.CharField(
        label='identifiant',
        max_length=25,
        widget=forms.TextInput(attrs={'placeholder': 'identifiant', 'class': 'inputLogin'}),
    )
    # be careful, forms.PasswordInput without widget won't show on screen...
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label='mot de passe'
    )
    # Add new caracteristic to password charfield
    password.widget.attrs.update({'placeholder': 'mot de passe', 'class': 'inputPwd'})

    class Meta:
        # change to custom user model 
        model = get_user_model()

def validate_game_name(value):
    if len(value) > 1 or len(value) <= 20:
        raise ValidationError(
            _("Votre nom de jeu doit contenir entre 1 et 20 caractères. "),
        )
    elif Game.object.filter(name=value, owner_uuid_id=request.user.id).exists():
        raise ValidationError(
            _("Vous avez déjà utilisé ce nom de jeu.")
        )
def validate_image_extension(value):
    import os
    ext = (os.path.splitext(value.name)[-1]).lower()
    if not ext in Pargen.objects.filter(pargen_type='IMAGE').values_list('value', flat=True):
        raise ValidationError(u'File not supported!')

def validate_document_extension(value):
    import os
    ext = (os.path.splitext(value.name)[-1]).lower()
    if not ext in Pargen.objects.filter(pargen_type='DOC').values_list('value', flat=True):
        raise ValidationError(u'File not supported!')

class GameCreateForm(forms.ModelForm):
    name = forms.CharField(
        validators=[validate_game_name],
        min_length=1, 
        max_length=20, 
        required=True,
        label="Nom du jeu: *",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image: *",
        required=True,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={'accept': ".jpg, .png"})
        )

    class Meta:
        model = Game
        fields = ['name', 'file']
        
class GameEditionForm(forms.ModelForm):
    name = forms.CharField(
        validators=[validate_game_name],
        min_length=1, 
        max_length=20, 
        required=True,
        label="Nom du jeu",
        widget=forms.TextInput(attrs={
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6  mx-auto align-center'
        }))
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image: ",
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={'accept': ".jpg, .png"})
        )

    class Meta:
        model = Game
        fields = ['name', 'file']


class UserGameChoicesForm(forms.Form):
    # Redefined ScenarioForm Method
    def __init__(self, user, *args, **kwargs):
        # Heritage
        super(UserGameChoicesForm, self).__init__(*args, **kwargs)
        # Redefined field "game"
        self.fields['game'] = forms.ChoiceField(
            label="Lier à un jeu",
            # lone way to use db data has value for choicefield.
            choices=[(key['name'], value['id']) for key, value in Game.objects.filter(owner_uuid_id=user).values()],
        )

    class Meta:
        model = Scenario
        fields = ['id_game_id_id']


class MultipleChoiceForm(forms.Form):
    npc_select = forms.ModelMultipleChoiceField(
        queryset=None,
        label="Pnj",
        required=False,
        widget=forms.Select(attrs={
            'class': 'multiple-npc-select col-xl-12 col-lg-12 col-md-12 col-xs-12',
        }))
    document_select = forms.ModelMultipleChoiceField(
        queryset=None,
        label="Pnj",
        required=False,
        widget=forms.Select(attrs={
            'class': 'multiple-document-select col-xl-12 col-lg-12 col-md-12 col-xs-12',
        }))
    npc_select = forms.ModelMultipleChoiceField(queryset=None)
    npc_select = forms.ModelMultipleChoiceField(queryset=None)

    # __init__ is redefined to get somes args.
    def __init__(self, user, selected_game_id = None, selected_scenario = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['npc_select'].queryset = Npc.objects.filter(owner_uuid_id=user, id_game_id_id=selected_game_id).only('name','id')
        self.fields['document_select'].queryset = Document.objects.filter(owner_uuid_id=user, id_game_id_id=selected_game_id).only('name','id')
        

class ScenarioCreateForm(forms.Form):
    name = forms.CharField(
        label="Nom",
        max_length=30,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'scenario-create-name',
            'class': 'form-control input-name col-xl-5 mx-auto align-items-center',
        }),
    )
    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorWidget(attrs={
            'id': 'create-description',
            'class': 'form-control input-description mx-auto'
        }),
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image",
        required=False,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={
            'accept': ".jpg, .png",
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6 btn btn-secondary  mx-auto align-center p-2',
            'name': 'file'
            })
        )
    class Meta:
        model = Scenario
        fields = ['name', 'description', 'npc_list']
        
        
class SceneCreateForm(forms.Form):
    name = forms.CharField(
        label="Nom",
        max_length=45,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'scene-create-name',
            'class': 'form-control input-name col-xl-5 mx-auto align-items-center'
        })
    )
    order = forms.IntegerField(
        label="Ordre",
        required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control input-description col-xl-1 col-md-3 col-sm-12',
            'value': '1',
            'min': '1',
            'default': '1'
        }),
    )
    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorWidget(attrs={
            'id': 'create-description',
            'class': 'form-control input-description mx-auto cke_editor_create-description'
        }),
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image   ",
        required=False,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={
            'accept': ".jpg, .png",
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6 btn btn-secondary  mx-auto align-center p-2',
            'name': 'file'
            })
        )
    
    class Meta:
        model = Scene
        fields = ['name', 'description', 'content', 'order']
        

class ChapterCreateForm(forms.Form):
    name = forms.CharField(
        label="Nom",
        max_length=30,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'chapter-create-name',
            'class': 'form-control input-name col-xl-5 mx-auto align-items-center',
        }),
    )
    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorWidget(attrs={
            'id': 'create-description',
            'class': 'form-control input-description mx-auto cke_editor_create-description'
        }),
    )
    order = forms.IntegerField(
        label="Ordre",
        required=True,
        widget=forms.NumberInput(attrs={
            'class': 'form-control input-description col-xl-1 col-md-3 col-sm-12',
            'id': 'id_order',
            'value': '1',
            'default': '1'
        }),
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image",
        required=False,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={
            'accept': ".jpg, .png",
            'class': 'form-control text-center col-10 col-sm-8 col-md-8 col-xl-6 btn btn-secondary  mx-auto align-center p-2',
            'name': 'file'
            })
        )
    
    class Meta:
        model = Chapter
        fields = ['name', 'description', 'order']
        
        
class UploadFileForm(forms.ModelForm):
    name = forms.CharField(
        label="Nom",
        max_length=45,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'document-create-name',
            'class': 'form-control input-name'
        }),
    )
    file = forms.FileField(
        label="Selectionnez un fichier:"
    )
    class Meta:
        model = Document
        fields = ['name', 'file']        
        
class UploadImageFileForm(forms.ModelForm):
    name = forms.CharField(
        label="Nom",
        max_length=45,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'document-create-name',
            'class': 'form-control input-name'
        }),
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image: *",
        required=True,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={'accept': ".jpg, .png"})
        )
    class Meta:
        model = Document
        fields = ['name', 'file']

class UploadDocumentFileForm(forms.ModelForm):
    name = forms.CharField(
        label="Nom",
        max_length=15,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'document-create-name',
            'class': 'form-control input-name'
        }),
    )
    file = forms.FileField(
        validators=[validate_document_extension],
        label="Selectionnez un document: *",
        required=True,
        help_text = "formats valides: .txt, .pdf",
        widget=forms.FileInput(attrs={'accept': ".txt, .pdf"})
        )
    class Meta:
        model = Document
        fields = ['name', 'file']
           
        
class ItemCreateForm(forms.ModelForm):
    
    #get ['.jpg', '.png'] pargen id.
    img_extensions = [value for value in Pargen.objects.filter(pargen_type='IMAGE').values_list('id', flat=True)]
    name = forms.CharField(
        label="Nom",
        max_length=30,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'item-create-name',
            'class': 'form-control input-name col-xl-5 mx-auto align-items-center',
        }),
        
    )
    
    image = forms.ModelChoiceField(
        label="Image",
        required=False,
        widget=forms.Select(attrs={
            'class': 'item-image-select col-xl-12 col-lg-12 col-md-12 col-xs-12',
        }),
        queryset=Document.objects.filter(id_pargen_type_id_id__in=img_extensions)
        
    )
    
    description = RichTextFormField(
        label="Description",
        required=False,
        widget=forms.TextInput(attrs={
            'id': 'item-create-description',
            'class': 'form-control input-description col-12'
        })
    )    
    effect = RichTextFormField(
        label="Effets",
        required=False,
        widget=forms.TextInput(attrs={
            'id': 'item-create-effect',
            'class': 'form-control input-effect col-12'
        })
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image: *",
        required=True,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={'accept': ".jpg, .png, .JPG, .PNG, .svg, .SVG"})
        )
    class Meta:
        model = Item
        fields = ['name', 'description']     
        
        
class LocationCreateForm(forms.ModelForm):
    
    #get ['.jpg', '.png'] pargen id.
    img_extensions = [value for value in Pargen.objects.filter(pargen_type='IMAGE').values_list('id', flat=True)]
    name = forms.CharField(
        label="Nom",
        max_length=30,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'location-create-name',
            'class': 'form-control input-name col-xl-5 mx-auto align-items-center',
        }),
        
    )
    
    image = forms.ModelChoiceField(
        label="Image",
        required=False,
        widget=forms.Select(attrs={
            'class': 'location-image-select col-xl-12 col-lg-12 col-md-12 col-xs-12',
        }),
        queryset=Document.objects.filter(id_pargen_type_id_id__in=img_extensions)
        
    )
    
    description = RichTextFormField(
        label="Description",
        required=False,
        widget=forms.TextInput(attrs={
            'id': 'location-create-description',
            'class': 'form-control input-description col-12'
        })
    )
    file = forms.FileField(
        validators=[validate_image_extension],
        label="Selectionnez une image: *",
        required=True,
        help_text = "formats valides: .jpg, .png",
        widget=forms.FileInput(attrs={'accept': ".jpg, .png"})
        )
    class Meta:
        model = Location
        fields = ['name', 'description']     
        
        
class NpcCreateForm(forms.ModelForm):
    
    #get ['.jpg', '.png'] pargen id.
    img_extensions = [value for value in Pargen.objects.filter(pargen_type='IMAGE').values_list('id', flat=True)]
    
    name = forms.CharField(
        label="Nom",
        max_length=45,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'npc-create-name',
            'class': 'form-control input-name col-xl-10 col-lg-10 col-md-12 col-xs-12'
        })
    )

    class Meta: 
        model = Npc
        fields = ['name' ]

        
    
class DetailedNoteCreateForm(forms.Form):
    name = forms.CharField(
    label="Titre de votre note",
    max_length=45,
    required=True,
    widget=forms.TextInput(attrs={
        'id': 'detailednote-create-name',
        'class': 'form-control input-name col-xl-5 mx-auto align-items-center',
        'placeholder': 'Votre titre ici...',
    })
    )
    note = forms.CharField(
        required=False,
        widget=CKEditorWidget(attrs={
            'id': 'create-detailed-note',
            'class': 'form-control col-xl-12 input-detaillednote mx-auto cke_editor_create-detailednote',
            'placeholder': 'test',
        }),
    )
    
    class Meta:
        model = DetailedNote
        fields = ['name', 'note']
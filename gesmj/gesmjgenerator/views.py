# -*- coding: utf-8 -*-
import json
import random

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_protect

from gesmjgenerator.forms import *
from gestion.classes.get_from_db import GetFromDb

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gesmj.settings")

def index(request, confirmation_msg=None):
    """
    :param request:
    :return: login_view.html or index.html
    View which show login_view page.
    """
    loginForm = LoginForm()
    return HttpResponse(render(request,'gesmjgenerator/index.html', {'loginForm': loginForm, 'confirmation_msg' : confirmation_msg if confirmation_msg != None else None}))


def create_user_view(request, error_msg=None):
    """
    :param request:
    :return: creat_user.html
    View which show create_user page.
    """
    loginForm = LoginForm()
    register_form = UserCreateForm()
    return HttpResponse(render(request, 'gesmjgenerator/create/user.html', {'register_form': register_form, 'loginForm': loginForm, 'error_msg': error_msg if error_msg != None else None}))

    
@login_required
@ensure_csrf_cookie
@csrf_protect
def dashboard(request, game_name=None, error=None, type=None, name=None, success=None, submit_error=None):
    """
    :param request:
    :return: if user is authenticated ; go to index.html ; else go to login_view.
    """
    if request.user.is_authenticated:
        userDb = GetFromDb(request.user)
        game_list_and_statistics = [{
            'id': key['id'],
            'name': key['name'],
            'file': key['file']} for key in Game.objects.filter(owner_uuid_id=request.user.id, deleted_at__isnull=True).values()]
        
        pargen_image_id = Pargen.objects.filter(pargen_type='IMAGE').values('id')
        
        last_game = Game.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Game.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_scenario = Scenario.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Scenario.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_chapter = Chapter.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Chapter.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_scene = Scene.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Scene.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_npc = Npc.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Npc.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_item = Item.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Item.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_location = Location.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if Location.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_document = Document.objects.filter(owner_uuid_id=request.user.id).exclude(id_pargen_type_id__in=pargen_image_id).latest('created_at') if Document.objects.filter(owner_uuid_id=request.user.id).exclude(id_pargen_type_id__in=pargen_image_id).exists() else  ''
        last_npc_template = NpcTemplate.objects.filter(owner_uuid_id=request.user.id).latest('created_at') if NpcTemplate.objects.filter(owner_uuid_id=request.user.id).exists() else  ''
        last_image = Document.objects.filter(owner_uuid_id=request.user.id, id_pargen_type_id__in=pargen_image_id).latest('created_at') if Document.objects.filter(owner_uuid_id=request.user.id, id_pargen_type_id__in=pargen_image_id).exists() else '' 
        informations = {
            'game': last_game,
            'scenario': last_scenario,
            'chapter': last_chapter,
            'scene': last_scene,
            'npc_template': last_npc_template,
            'npc': last_npc,
            'item': last_item,
            'location': last_location,
            'document': last_document,
            'image': last_image
        }
        return render(request, 'gesmjgenerator/dashboard.html', {'user_game_list':game_list_and_statistics, 'informations':  informations, 'game_name': game_name })
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_game_view(request):
    if request.user.is_authenticated:
        return render(request, 'gesmjgenerator/create/game.html', {'gameCreateForm': GameCreateForm})
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


def update_game_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            old_game_name = Game.objects.filter(owner_uuid_id=request.user.id, name=game_name, deleted_at__isnull=True).values_list('name', flat=True)
            old_file_name = Game.objects.filter(owner_uuid_id=request.user.id, name=game_name, deleted_at__isnull=True).values_list('file', flat=True)
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/edit/game_edition.html', {
                'gameCreateForm': GameEditionForm,  
                'game_id': game_id,
                'old_game_name': old_game_name[0],
                'old_file_name': old_file_name[0],
                'note': note[0] if note != '' else ''
                })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
      
      
@login_required
@ensure_csrf_cookie
@csrf_protect      
def display_all_research_result(request, game_name, search_keyword, search_word):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            search_in_db = [ element for element in json.loads(GetFromDb(request.user.id, game_id).search_in_db_engine(search_word, search_keyword)) ]
            if not search_in_db is None:
                return render(request, 'gesmjgenerator/display/search_result.html', {
                    'game_name':game_name, 
                    'game_id': game_id,
                    'search_result' : search_in_db,
                    'search_keyword' : search_keyword,
                    'note': note[0] if note != '' else '',
                })
            else:
                return display_game_content(game_name)
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_scenario_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/create/scenario.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'ScenarioCreateForm': ScenarioCreateForm,
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_scenario_list_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            scenario_list = Scenario.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values()
            return render(request, 'gesmjgenerator/display/scenario_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'scenario_list': scenario_list,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_scenario_view(request, game_name, pk):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            if Scenario.objects.filter(id=pk , owner_uuid_id=request.user.id, id_game_id_id=game_id).exists():
                scenario_content = Scenario.objects.filter(id=pk , owner_uuid_id=request.user.id, id_game_id_id=game_id).values()
                scenario_data = [ entry for entry in scenario_content ]
                scenario_id = Scenario.objects.filter(id=scenario_data[0]['id'] , owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
                document_id = Document.objects.filter(name=scenario_data[0]['image'], owner_uuid_id=request.user.id).values_list('id', flat=True)
                chapter_content = Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id__in=scenario_id).order_by('order').values('name','id','image','color', 'description')
                return render(request, 'gesmjgenerator/display/scenario_content.html', {
                        'game_name' : game_name,
                        'game_id': game_id,
                        'scenario_content': scenario_content,
                        'chapter_content' : chapter_content,
                        'scenario_id': scenario_id[0],
                        'document_id': document_id,
                        'old_file_name' : scenario_data[0]['image'],
                        'note': note[0] if note != '' else ''
                    })
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
                
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_chapter_list_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            scenario_id_list = Scenario.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
            chapter_id_list = Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id__in=scenario_id_list).values_list('id', flat=True)
            chapter_list = Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id__in=scenario_id_list).order_by('order').values()
            scenarios = Scenario.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, id__in=scenario_id_list).values('id','name')
            scenario_list = [entry for entry in scenarios]
            for scenario in scenario_list:
                count_variable = {'chapter_count': Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id=scenario['id']).count()}
                if count_variable:
                    scenario.update(count_variable)
                else:
                    scenario.update({'count': 0})
            return render(request, 'gesmjgenerator/display/chapter_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'chapter_list': chapter_list,
                'scenario_id_list': scenarios,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_chapter_view(request, game_name, pk):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            if Chapter.objects.filter(id=pk , owner_uuid_id=request.user.id).exists():
                
                chapter_content = Chapter.objects.filter(id=pk , owner_uuid_id=request.user.id).values()
                chapter_data = [ entry for entry in chapter_content ]
                chapter_id = Chapter.objects.filter(id=chapter_data[0]['id'] , owner_uuid_id=request.user.id).values_list('id', flat=True)
                document_id = Document.objects.filter(name=chapter_data[0]['image'], owner_uuid_id=request.user.id).values_list('id', flat=True)
                chapter_content = Chapter.objects.filter(id=chapter_data[0]['id'], owner_uuid_id=request.user.id).order_by('order').values('name','id','image','color', 'description')
                scene_content = Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id__in=chapter_id).order_by('order').values('name','id','image','color', 'description')
                return render(request, 'gesmjgenerator/display/chapter_content.html', {
                        'game_name' : game_name,
                        'game_id': game_id,
                        'chapter_content': chapter_content,
                        'chapter_id': chapter_id[0],
                        'document_id': document_id,
                        'old_file_name' : chapter_data[0]['image'],
                        'scene_content' : scene_content,    
                        'note': note[0] if note != '' else ''
                    })
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_scene_list_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            scenario_id_list = Scenario.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('id', flat=True)
            chapter_id_list = Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id__in=scenario_id_list).values_list('id', flat=True)
            scene_list = Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id__in=chapter_id_list).order_by('order').values()
            chapters = Chapter.objects.filter(owner_uuid_id=request.user.id, id__in=chapter_id_list, deleted_at__isnull=True).order_by('order').values()
            scene_count = Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id__in=chapter_id_list).count()
            chapter_list = [entry for entry in chapters]
            for chapter in chapter_list:
                count_variable = {'scene_count': Scene.objects.filter(owner_uuid_id=request.user.id, id_chapter_id_id=chapter['id']).count()}
                if count_variable:
                    chapter.update(count_variable)
                else:
                    chapter.update({'count': 0})
                    
            return render(request, 'gesmjgenerator/display/scene_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'scene_list': scene_list,
                'chapter_id_list': chapter_list,
                'scene_count': scene_count,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_scene_view(request, game_name, pk):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            if Scene.objects.filter(id=pk, owner_uuid_id=request.user.id).exists():
                
                npc_id_in_scene = NpcInScene.objects.filter(id_scene_id_id=pk, id_game_id_id=game_id, deleted_at__isnull=True).values_list('id_npc_id_id', flat=True)
                npcs = Npc.objects.filter(owner_uuid_id=request.user.id, id__in=npc_id_in_scene, id_game_id_id=game_id, deleted_at__isnull=True).values('id', 'name', 'image')
                full_npc_list = Npc.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values()
                npc_count = Npc.objects.filter(id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).count()
                
                location_id_in_scene = LocationInScene.objects.filter(id_scene_id_id=pk, id_game_id_id=game_id, deleted_at__isnull=True).values_list('id_location_id_id', flat=True)
                locations = Location.objects.filter(owner_uuid_id=request.user.id, id__in=location_id_in_scene , id_game_id_id=game_id, deleted_at__isnull=True).values()
                full_location_list = Location.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values('id', 'name')
                location_count = Location.objects.filter(id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).count()
                scene_content = Scene.objects.filter(id=pk, owner_uuid_id=request.user.id).order_by('order').values('name','id','image','description')
                scene_data = [ entry for entry in scene_content]
                document_id = Document.objects.filter(name=scene_data[0]['image'], owner_uuid_id=request.user.id).values('id')
                
                return render(request, 'gesmjgenerator/display/scene_content.html', {
                        'game_name' : game_name,
                        'game_id': game_id,
                        'scene_content' : scene_content,
                        'npc_content' : npcs,
                        'npc_selection' : full_npc_list,
                        'npc_count': npc_count,
                        'location_content': locations,
                        'location_selection': full_location_list,
                        'location_count': location_count,
                        'scene_id': scene_content[0]['id'],
                        'document_id' : document_id if not document_id is None else '',
                        'old_file_name': scene_content[0]['image'],
                        'note': note[0] if note != '' else ''
                    })
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_item_list_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            item_list = Item.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id)
            return render(request, 'gesmjgenerator/display/item_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'item_list': item_list,
                'item_id': item_list.values('id'),
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_item_view(request, game_name, pk):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            if Item.objects.filter(id=pk, owner_uuid_id=request.user.id).exists():
                item_content = Item.objects.filter(id=pk, owner_uuid_id=request.user.id)
                document_id = DocumentInItem.objects.filter(id_item_id_id__in=item_content.values('id')).values('id')
                
                npc = ItemInNpc.objects.filter(id_item_id_id=pk, id_game_id_id=game_id)
                return render(request, 'gesmjgenerator/display/item_content.html', {
                        'game_name' : game_name,
                        'game_id': game_id,
                        'item_content' : item_content,
                        'item_id': item_content.values_list('id', flat=True)[0],
                        'npc_content' : Npc.objects.filter(id__in=npc.values('id_npc_id_id'), id_game_id_id=game_id).values('id', 'name', 'image'),
                        'npc_count': npc.count(),
                        'document_id' : document_id if not document_id is None else '',
                        'old_file_name': item_content.values('image')[0]['image'],
                        'note': note[0] if note != '' else ''
                    })
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_location_list_view(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            location_list = Location.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id)
            return render(request, 'gesmjgenerator/display/location_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'location_list': location_list,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_location_view(request, game_name, pk):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            
            if Location.objects.filter(id=pk, owner_uuid_id=request.user.id).exists():
                
                location_content = Location.objects.filter(id=pk, owner_uuid_id=request.user.id).values('name','description','id','image','color')
                scene_in_location_id = LocationInScene.objects.filter(id_location_id_id=pk, id_game_id_id=game_id).values('id_scene_id_id')
                scene_content = Scene.objects.filter(id__in=scene_in_location_id, owner_uuid_id=request.user.id).order_by('order').values('name', 'id', 'image', 'color')
                location_data = [ entry for entry in location_content]
                document_id = Document.objects.filter(name=location_data[0]['image'], owner_uuid_id=request.user.id).values_list('id', flat=True)
                
                return render(request, 'gesmjgenerator/display/location_content.html', {
                        'game_name' : game_name,
                        'game_id': game_id,
                        'location_id' : int(pk),
                        'location_content' : location_content,
                        'scene_content' : scene_content,
                        'old_file_name' : location_data[0]['image'],
                        'document_id' : document_id,
                        'note': note[0] if note != '' else '',
                    }) 
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
  
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_upload_game_document(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/create/game_document.html', context={
                'game_name':game_name, 
                'game_id': game_id,
                'UploadDocumentFileForm': UploadDocumentFileForm,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
               
                        
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_game_gallery_pictures(request, game_name):
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            db = GetFromDb(request.user, game_name, "IMAGE")
            # get game_id
            game_id = db.get_game_id()
            
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            picture_pargen_id = Pargen.objects.filter(pargen_type="IMAGE", deleted_at__isnull=True).values_list('id', flat=True)
            keyword_picture_id = db.get_keyword_id("vide")
            picture_id_count = Document.objects.filter(keyword__id_pargenKeyword_id_id=keyword_picture_id, id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).count()
            picture_id_list = Document.objects.filter(keyword__id_pargenKeyword_id_id=keyword_picture_id, id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values()
            
            picture_array = {}
            aria_controls_id_list = ""
            for i in range(0, len(picture_id_list),1):
                aria_controls_id_list += "display-picture-card-{} ".format(picture_id_list[i]['id'])
            
            for i in range(0, len(picture_id_list),1):
                if i == 0:
                    picture_array.update({ i : {
                        'count' : picture_id_count,
                        'category_title': 'images non-répertoriées',
                        'aria_controls_id_list' : aria_controls_id_list,
                        'class_name' : 'document',
                        'id' : picture_id_list[i]['id'],
                        'name' : 'Nul part',
                        'document_id' : picture_id_list[i]['id'],
                        'document_name' : picture_id_list[i]['name'],
                        'document_short_name' : picture_id_list[i]['short_name'],
                        'document_created_at' : picture_id_list[i]['created_at'],
                    },})
                else:
                    picture_array.update({ i : {
                        'id' : picture_id_list[i]['id'],
                        'category_title': 'empty',
                        'class_name' : 'document',
                        'id' : picture_id_list[i]['id'],
                        'name' : 'Nul part',
                        'document_id' : picture_id_list[i]['id'],
                        'document_name' : picture_id_list[i]['name'],
                        'document_short_name' : picture_id_list[i]['short_name'],
                        'document_created_at' : picture_id_list[i]['created_at'],
                },})
            
            
            # GET ALL PARGEN ID WHICH ARE IN RELATION WITH PICTURES
            keyword_scenario_id = db.get_keyword_id("scenario")
            scenario_id_list = DocumentInScenario.objects.all().filter(id_game_id_id=game_id).exclude(id_scenario_id_id__name="Empty", id_scenario_id_id__name__isnull=True).values(
                'id_scenario_id_id',
                'id_scenario_id_id__name',
                'id_document_id_id',
                'id_document_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(scenario_id_list),1):
                aria_controls_id_list += "display-scenario-card-{} ".format(scenario_id_list[i]['id_document_id_id'])
                
            scenario_array = {}
            for i in range(0, len(scenario_id_list),1):
                if i == 0:
                    scenario_array.update({ i : {
                        'category_title': 'scenarios',
                        'class_name' : 'scenario',
                        'aria_controls_id_list' : aria_controls_id_list,
                        'id' : scenario_id_list[i]['id_scenario_id_id'],
                        'name' : scenario_id_list[i]['id_scenario_id_id__name'],
                        'document_id' : scenario_id_list[i]['id_document_id_id'],
                        'document_name' : scenario_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : scenario_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : scenario_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    scenario_array.update({ i : {
                        'id' : scenario_id_list[i]['id_scenario_id_id'],
                        'category_title': 'scenarios',
                        'class_name' : 'scenario',
                        'name' : scenario_id_list[i]['id_scenario_id_id__name'],
                        'document_id' : scenario_id_list[i]['id_document_id_id'],
                        'document_name' : scenario_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : scenario_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : scenario_id_list[i]['id_document_id_id__created_at'],
                },})
                
            
            keyword_id = db.get_keyword_id("vide")
            pictures = Document.objects.filter(keyword__id_pargenKeyword_id_id=keyword_id, id_pargen_type_id_id__in=picture_pargen_id, deleted_at__isnull=True, id_game_id_id=game_id).exclude(name="Empty", name__isnull=True).values()

            keyword_id = db.get_keyword_id("chapitre")
            chapter_id_list = DocumentInChapter.objects.all().filter(id_game_id_id=game_id).exclude(id_chapter_id_id__name="Empty", id_chapter_id_id__name__isnull=True).values(
                'id_chapter_id_id',
                'id_chapter_id_id__name',
                'id_document_id_id', 
                'id_document_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(chapter_id_list),1):
                aria_controls_id_list += "display-chapter-card-{} ".format(chapter_id_list[i]['id_document_id_id'])
            
            chapter_array = {}
            for i in range(0, len(chapter_id_list),1):
                if i == 0:
                    chapter_array.update({ i : {
                        'category_title': 'chapitres',
                        'class_name' : 'chapter',
                        'aria_controls_id_list' :aria_controls_id_list,
                        'id' : chapter_id_list[i]['id_chapter_id_id'],
                        'name' : chapter_id_list[i]['id_chapter_id_id__name'],
                        'document_id' : chapter_id_list[i]['id_document_id_id'],
                        'document_name' : chapter_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : chapter_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : chapter_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    chapter_array.update({ i : {
                        'id' : chapter_id_list[i]['id_chapter_id_id'],
                        'category_title': 'chapitres',
                        'class_name' : 'chapter',
                        'name' : chapter_id_list[i]['id_chapter_id_id__name'],
                        'document_id' : chapter_id_list[i]['id_document_id_id'],
                        'document_name' : chapter_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : chapter_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : chapter_id_list[i]['id_document_id_id__created_at'],
                    },})
            
            
            keyword_id = db.get_keyword_id("scene")
            scene_id_list = DocumentInScene.objects.all().filter(id_game_id_id=game_id).exclude(id_scene_id_id__name="Empty", id_scene_id_id__name__isnull=True).values(
                'id_scene_id_id',
                'id_scene_id_id__name',
                'id_document_id_id',
                'id_document_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(scene_id_list),1):
                aria_controls_id_list += "display-scene-card-{} ".format(scene_id_list[i]['id_document_id_id'])
            
            scene_array = {}
            for i in range(0, len(scene_id_list),1):
                if i == 0:
                    scene_array.update({ i : {
                        'category_title': 'scenes',
                        'class_name' : 'scene',
                        'aria_controls_id_list' : aria_controls_id_list,
                        'id' : scene_id_list[i]['id_scene_id_id'],
                        'name' : scene_id_list[i]['id_scene_id_id__name'],
                        'document_id' : scene_id_list[i]['id_document_id_id'],
                        'document_name' : scene_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : scene_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : scene_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    scene_array.update({ i : {
                        'category_title': 'scenes',
                        'class_name' : 'scene',
                        'id' : scene_id_list[i]['id_scene_id_id'],
                        'name' : scene_id_list[i]['id_scene_id_id__name'],
                        'document_id' : scene_id_list[i]['id_document_id_id'],
                        'document_name' : scene_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : scene_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : scene_id_list[i]['id_document_id_id__created_at'],
                    },})
                    
            
            keyword_id = db.get_keyword_id("lieu")
            location_id_list = DocumentInLocation.objects.all().filter(id_game_id_id=game_id).exclude(id_location_id_id__name="Empty", id_location_id_id__name__isnull=True).values(
                'id_location_id_id',
                'id_location_id_id__name',
                'id_document_id_id',
                'id_document_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(location_id_list),1):
                aria_controls_id_list += "display-location-card-{} ".format(location_id_list[i]['id_document_id_id'])
            
            location_array = {}
            for i in range(0, len(location_id_list),1):
                if i == 0:
                    location_array.update({ i : {
                        'category_title': 'lieux',
                        'class_name' : 'location',
                        'aria_controls_id_list' : aria_controls_id_list,
                        'id' : location_id_list[i]['id_location_id_id'],
                        'name' : location_id_list[i]['id_location_id_id__name'],
                        'document_id' : location_id_list[i]['id_document_id_id'],
                        'document_name' : location_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : location_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : location_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    location_array.update({ i : {
                        'category_title': 'lieux',
                        'class_name' : 'location',
                        'id' : location_id_list[i]['id_location_id_id'],
                        'name' : location_id_list[i]['id_location_id_id__name'],
                        'document_id' : location_id_list[i]['id_document_id_id'],
                        'document_name' : location_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : location_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : location_id_list[i]['id_document_id_id__created_at'],
                    },})
            
            keyword_id = db.get_keyword_id("pnj")
            npc_id_list = DocumentInNpc.objects.all().filter(id_game_id_id=game_id).exclude(id_npc_id_id__name="Empty", id_npc_id_id__name__isnull=True).values(
                'id_npc_id_id',
                'id_document_id_id',
                'id_npc_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(npc_id_list),1):
                aria_controls_id_list += "display-npc-card-{} ".format(npc_id_list[i]['id_document_id_id'])
            
            npc_array = {}
            for i in range(0, len(npc_id_list),1):
                if i == 0:
                    npc_array.update({ i : {
                        'category_title': 'personnages non-joueurs',
                        'class_name' : 'npc',
                        'aria_controls_id_list': aria_controls_id_list,
                        'id' : npc_id_list[i]['id_npc_id_id'],
                        'name' : npc_id_list[i]['id_npc_id_id__name'],
                        'document_id' : npc_id_list[i]['id_document_id_id'],
                        'document_name' : npc_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : npc_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : npc_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    npc_array.update({ i : {
                        'category_title': 'personnages non-joueurs',
                        'class_name' : 'npc',
                        'id' : npc_id_list[i]['id_npc_id_id'],
                        'name' : npc_id_list[i]['id_npc_id_id__name'],
                        'document_id' : npc_id_list[i]['id_document_id_id'],
                        'document_name' : npc_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : npc_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : npc_id_list[i]['id_document_id_id__created_at'],
                    },})
            
            keyword_id = db.get_keyword_id("objet")
            item_id_list = DocumentInItem.objects.all().filter(id_game_id_id=game_id).exclude(id_item_id_id__name="Empty", id_item_id_id__name__isnull=True).values(
                'id_item_id_id',
                'id_item_id_id__name',
                'id_document_id_id', 
                'id_document_id_id__name', 
                'id_document_id_id__short_name', 
                'id_document_id_id__created_at' )
            
            aria_controls_id_list = ""
            for i in range(0, len(item_id_list),1):
                aria_controls_id_list += "display-item-card-{} ".format(item_id_list[i]['id_document_id_id'])
            
            item_array = {}
            for i in range(0, len(item_id_list),1):
                if i == 0:
                    item_array.update({ i : {
                        'id' : item_id_list[i]['id_item_id_id'],
                        'category_title': 'objets',
                        'class_name' : 'item',
                        'aria_controls_id_list' : aria_controls_id_list,
                        'name' : item_id_list[i]['id_item_id_id__name'],
                        'document_id' : item_id_list[i]['id_document_id_id'],
                        'document_name' : item_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : item_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : item_id_list[i]['id_document_id_id__created_at'],
                    },})
                else:
                    item_array.update({ i : {
                        'category_title': 'objets',
                        'class_name' : 'item',
                        'id' : item_id_list[i]['id_item_id_id'],
                        'name' : item_id_list[i]['id_item_id_id__name'],
                        'document_id' : item_id_list[i]['id_document_id_id'],
                        'document_name' : item_id_list[i]['id_document_id_id__name'],
                        'document_short_name' : item_id_list[i]['id_document_id_id__short_name'],
                        'document_created_at' : item_id_list[i]['id_document_id_id__created_at'],
                    },})
            
            data_dict = {
                'pictures': picture_array,
                'scenarios' : scenario_array,
                'chapters' : chapter_array,
                'scenes': scene_array,
                'locations': location_array,
                'npcs' : npc_array,
                'items' : item_array
            }
                    
            return render(request, 'gesmjgenerator/display/game_gallery_pictures.html', context={
                'game_name': game_name,
                'data_dict' : data_dict,
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:game_doesnt_exist_view'))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def create_npc_template_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create npc view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            picture_allowed_extension_id = Pargen.objects.filter(pargen_type="IMAGE", deleted_at__isnull=True).values_list('id', flat=True)
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            linked_picture_id_list = Document.objects.filter(id_game_id_id=game_id, id_pargen_type_id_id__in=picture_allowed_extension_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            
            return render(request, 'gesmjgenerator/create/npc_template.html',{
                'game_name': game_name,
                'game_id': game_id,
                'note': note[0] if note != '' else ''
            })    
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

@login_required
@ensure_csrf_cookie
@csrf_protect
def display_npc_template_list_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create npc view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            picture_allowed_extension_id = Pargen.objects.filter(pargen_type="IMAGE", deleted_at__isnull=True).values_list('id', flat=True)
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            linked_picture_id_list = Document.objects.filter(id_game_id_id=game_id, id_pargen_type_id_id__in=picture_allowed_extension_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            
            return render(request, 'gesmjgenerator/create/npc_template.html',{
                'game_name': game_name,
                'game_id': game_id,
                'note': note[0] if note != '' else ''
            })
        else:
            response_data = {'error':True, 'msg': 'Ce jeu auquel vous essayez d\'acceder n\'existe pas.'}
            return render(request, 'gesmjgenerator/dashboard.html', {'response_data': response_data})
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})

@login_required
@ensure_csrf_cookie
@csrf_protect
def create_document_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create npc view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            document_allowed_extension_id = Pargen.objects.filter(pargen_type="DOC", deleted_at__isnull=True).values_list('id', flat=True)
            linked_picture_id_list = Document.objects.filter(id_game_id_id=game_id, id_pargen_type_id_id__in=document_allowed_extension_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values_list('id', flat=True)
            
            return render(request, 'gesmjgenerator/create/npc.html',{
                'game_name': game_name,
                'game_id': game_id,
                'note': note[0] if note != '' else ''
            })
        else:
            response_data = {'error':True, 'msg': 'Ce jeu auquel vous essayez d\'acceder n\'existe pas.'}
            return render(request, 'gesmjgenerator/dashboard.html', {'response_data': response_data})
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@xframe_options_sameorigin
def display_selected_document(request, game_name, pk):
    """
    :param request:
    :param game_name
    :return: display pdf.html view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            if isinstance(pk, int) and pk > 0:
                note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
                if Document.objects.filter(id=pk, id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
                    document_name = Document.objects.filter(id=pk, id_game_id_id=game_id, owner_uuid_id=request.user.id).values_list('name', flat=True)
                    return render(request, 'gesmjgenerator/display/pdf.html',{
                        'game_name': game_name,
                        'short_name': '',
                        'document_id': pk,
                        'document_name': document_name[0],
                        'game_id': game_id,
                        'note': note[0] if note != '' else '',
                    })
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
@xframe_options_sameorigin
def display_document_list_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: display document_list.html view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET DOCUMENT DATA 
            document_allowed_extension_id = Pargen.objects.filter(pargen_type="DOC", deleted_at__isnull=True).values_list('id', flat=True)
            linked_documents = Document.objects.filter(id_game_id_id=game_id, id_pargen_type_id_id__in=document_allowed_extension_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values()
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            
            return render(request, 'gesmjgenerator/display/document_content_list.html',{
                'game_name': game_name,
                'game_id': game_id,
                'linked_documents': linked_documents,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
            
            
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_game_content(request, game_name):
    """
    :param request:
    :param game_name
    :return: game content choice view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id).exists():
            # GET GAME ID
            userDb = GetFromDb(request.user)
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            scenario_id_list = Scenario.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values_list('id', flat=True)
            scenario_id_count = Scenario.objects.filter(id__in=scenario_id_list, deleted_at__isnull=True).count()
            chapter_id_count = Chapter.objects.filter(owner_uuid_id=request.user.id, id_scenario_id_id__in=scenario_id_list, deleted_at__isnull=True).count()
            npc_template_count = NpcTemplate.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).count()
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else '' 
            return render(request, 'gesmjgenerator/display/game_content_list.html',{
                'linked_scenario_count': scenario_id_count,
                'linked_chapter_count' : chapter_id_count,
                'game_name': game_name,
                'game_id': game_id,
                'linked_npc_template_count': int(npc_template_count) if npc_template_count != '' else 0, 
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_chapter_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create chapter view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            scenario_list = Scenario.objects.filter(id_game_id_id=game_id, deleted_at__isnull=True).values('id', 'name')
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/create/chapter.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'ChapterCreateForm': ChapterCreateForm,
                'scenario_list': scenario_list,
                'note': note[0] if note != '' else ''
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_scene_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create scene view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            scenario_id_list = Scenario.objects.only('id').filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True)
            chapter_list = Chapter.objects.filter(id_scenario_id_id__in=scenario_id_list, deleted_at__isnull=True).values('id', 'name')
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
    
            return render(request, 'gesmjgenerator/create/scene.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'SceneCreateForm': SceneCreateForm,
                'chapter_list': chapter_list,
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_item_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create item view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
    
            return render(request, 'gesmjgenerator/create/item.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'ItemCreateForm': ItemCreateForm,
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_location_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create item view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
    
            return render(request, 'gesmjgenerator/create/location.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'LocationCreateForm': LocationCreateForm,
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_note_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: create note view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            noteCreateForm = DetailedNoteCreateForm
            return render(request, 'gesmjgenerator/create/note.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'noteCreateForm': noteCreateForm, 
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def display_note_list_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: display note list view.
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            # GET GAME ID
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            # GET GAME'S NOTE
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            detailed_note_dict = DetailedNote.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list()
            return render(request, 'gesmjgenerator/display/detailed_note_list.html', {
                'game_name':game_name, 
                'game_id': game_id,
                'detailed_note_dict': detailed_note_dict, 
                'note': note[0] if note != '' else '',
            })
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})


@login_required
@ensure_csrf_cookie
@csrf_protect
def npc_template_selection(request, game_name):
    """
    :param request:
    :param game_name
    :return: display npc template selection
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            npc_template_list = NpcTemplate.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values('id', 'name')
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/display/npc_template_list.html', {'game_name': game_name, 'npc_template_list': npc_template_list, 'note': note[0] if note != '' else ''})
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
    
@login_required
@ensure_csrf_cookie
@csrf_protect
def create_npc_view(request, game_name, template_id):
    """
    :param request:
    :param game_name
    :return: display npc creation view.
    if selected_template_id is not None, display selected template
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            if NpcTemplate.objects.filter(id=template_id, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).exists():
                template_data_list = [x for x in list(NpcTemplate.objects.filter(id=template_id, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values())]
                item_data_list = Item.objects.filter(id_game_id_id=game_id, owner_uuid_id=request.user.id, deleted_at__isnull=True).values()
                competence_option = template_data_list[0]['statistics']['skills_option'] if ('number' in template_data_list[0]['statistics']['skills_option'] or 'far' in template_data_list[0]['statistics']['skills_option']) else 'number'
                attribute_option = template_data_list[0]['statistics']['attributes_option'] if ('number' in template_data_list[0]['statistics']['attributes_option'] or 'far' in template_data_list[0]['statistics']['attributes_option']) else 'number'
                note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
                npc = {
                    'game_name': game_name,
                    'attributes' : [{ 'name': name ,'formated_name': name.replace(' ','_')+"_"+str(random.randint(1,100000))} for name in template_data_list[0]['statistics']['attributes_names']],
                    'skills': [{ 'name': name ,'formated_name': name.replace(' ','_')+"_"+str(random.randint(1,100000))} for name in template_data_list[0]['statistics']['skills_names']],
                    'skills_option': competence_option,
                    'attributes_option': attribute_option,
                    'skills_values': int(template_data_list[0]['statistics']['skills_values']),
                    'attributes_values': int(template_data_list[0]['statistics']['attributes_values']),
                    'skills_values_range': range(1,int(template_data_list[0]['statistics']['skills_values'])),
                    'attributes_values_range': range(1,int(template_data_list[0]['statistics']['attributes_values'])),
                    'character_informations': [{ 'name': name ,'formated_name': name.replace(' ','_')+"_"+str(random.randint(1,100000))} for name in template_data_list[0]['background']['npc_informations']],
                    'character_background': [{ 'name': name ,'formated_name': name.replace(' ','_')+"_"+str(random.randint(1,100000))} for name in template_data_list[0]['background']['npc_descriptions']],
                    'text_font': template_data_list[0]['style']['text_font'],
                    'text_font_size': template_data_list[0]['style']['text_font_size'],
                    'text_font_color': template_data_list[0]['style']['text_font_color'],
                    'text_font_weight': template_data_list[0]['style']['text_font_style_first'],
                    'text_font_style': template_data_list[0]['style']['text_font_style_second'],
                    'title_font': template_data_list[0]['style']['title_font'],
                    'title_font_size': template_data_list[0]['style']['title_font_size'],
                    'title_font_color': template_data_list[0]['style']['title_font_color'],
                    'title_font_weight': template_data_list[0]['style']['title_font_style_first'],
                    'title_font_style': template_data_list[0]['style']['title_font_style_second'],
                    'foreground_color': template_data_list[0]['style']['foreground_color'],
                    'background_color': template_data_list[0]['style']['background_color'],
                    'inventory_range': range(0,24),
                    'item_list': item_data_list,
                    'template_id': template_id
                }
                
                return render(request, 'gesmjgenerator/create/npc.html', {'npc' : npc, 'game_name': game_name, 'note': note[0] if note != '' else ''})
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
    
        
@login_required
@ensure_csrf_cookie
@csrf_protect
def display_npc_list_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: display npc creation view.
    if selected_template_id is not None, display selected template
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            npcs = Npc.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values()
            note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
            return render(request, 'gesmjgenerator/display/npc_list.html', {'npcs': npcs, 'game_name': game_name, 'note': note[0] if note != '' else ''})
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
        

@login_required
@ensure_csrf_cookie
@csrf_protect
def display_selected_npc_view(request, game_name, pk):
    """
    :param request:
    :param game_name
    :return: display selected npc sheet and display views
    """
    if request.user.is_authenticated:
        if Game.objects.filter(name=game_name, owner_uuid_id=request.user.id, deleted_at__isnull=True).exists():
            game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
            if Npc.objects.filter(id=int(pk), owner_uuid_id=request.user.id, id_game_id_id=game_id).exists():
                npc = Npc.objects.filter(id=int(pk), owner_uuid_id=request.user.id, id_game_id_id=game_id).values().first()
                template_id = npc['id_template_id_id']
                template_data = NpcTemplate.objects.get(id=int(template_id), owner_uuid_id=request.user.id, id_game_id_id=game_id)
                items_image_name = []
                for key, value in npc['inventory'].items():
                    items_image_name.append(value)
                npc_items = {}
                    
                inventory = Item.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, image__in=items_image_name, deleted_at__isnull=True).values()
                items = Item.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values()
                template_data_list = [x for x in list(NpcTemplate.objects.filter(id=template_id, owner_uuid_id=request.user.id, id_game_id_id=game_id, deleted_at__isnull=True).values())]
                note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
                scenes_id = NpcInScene.objects.filter(id_npc_id_id=npc['id'], id_game_id_id=game_id).values_list('id_scene_id_id', flat=True)
                scene_data = Scene.objects.filter(id__in=scenes_id, owner_uuid_id=request.user.id, id_game_id_id=game_id).values('name', 'image', 'color', 'id')
                locations_id = NpcInLocation.objects.filter(id_npc_id_id=npc['id'], id_game_id_id=game_id).values_list('id_location_id_id', flat=True)
                location_data = Location.objects.filter(id__in=locations_id, owner_uuid_id=request.user.id, id_game_id_id=game_id).values('name', 'image', 'color', 'id')
                location_full_list =  Location.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values('name', 'image', 'color', 'id')
                location_count =  Location.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).count()
                data = {
                    'name': npc['name'],
                    'npc_id': npc['id'],
                    'style' : template_data.style,
                    'skills' : [{'name': " ".join(key.split('_')[:-1]) if key.split('_')[-1].isdigit() else key, 'formated_name': key, 'value' : value } for key, value in npc['skills'].items()],
                    'skills_option': template_data.statistics['skills_option'],
                    'skills_max_values': template_data_list[0]['statistics']['skills_values'],
                    'attributes' : [{'name': key.split('_')[:-1][0] if key.split('_')[-1].isdigit() else key, 'formated_name': key, 'value' : value } for key, value in npc['attributes'].items()],
                    'attributes_option': template_data.statistics['attributes_option'],
                    'attributes_max_values': template_data_list[0]['statistics']['attributes_values'],
                    'skills_values_range': range(1,int(template_data.statistics['skills_values'])+1),
                    'attributes_values_range': range(1,int(template_data.statistics['attributes_values'])+1),
                    'image': npc['image'],
                    'npc_informations': [{ 'name': " ".join(name.split('textfield-')[1].split('_')[:-1][:len(name)-1] if name.split('_')[-1].isdigit() else name) if "textfield-" in name else name ,
                                          'formated_name': "".join(name.split('textfield-')[1]) if "textfield-" in name else name, 
                                          'value': value} for name, value in npc['information'].items()],
                    'npc_descriptions': [{ 'name': " ".join(name.split('textfield-')[1].split('_')[:-1][:len(name)-1] if name.split('_')[-1].isdigit() else name) if "textfield-" in name else name ,
                                          'formated_name': "".join(name.split('textfield-')[1]) if "textfield-" in name else name, 
                                          'value': value} for name, value in npc['description'].items()],
                    'npc_inventory': [{
                        'item_slot': int(key.split('-')[-1]) if value != '' else '', 
                        'slot_name': key, 
                        'item_id': Item.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id, image=value, deleted_at__isnull=True).values_list('id', flat=True).first(),
                        'value': value
                        } for key, value in npc['inventory'].items()],
                    'inventory': npc_items,
                    'inventory_range': range(0,int(24)),
                    'items_list': items,
                    'text_font': template_data_list[0]['style']['text_font'],
                    'text_font_size': template_data_list[0]['style']['text_font_size'],
                    'text_font_color': template_data_list[0]['style']['text_font_color'],
                    'text_font_weight': template_data_list[0]['style']['text_font_style_first'],
                    'text_font_style': template_data_list[0]['style']['text_font_style_second'],
                    'title_font': template_data_list[0]['style']['title_font'],
                    'title_font_size': template_data_list[0]['style']['title_font_size'],
                    'title_font_color': template_data_list[0]['style']['title_font_color'],
                    'title_font_weight': template_data_list[0]['style']['title_font_style_first'],
                    'title_font_style': template_data_list[0]['style']['title_font_style_second'],
                    'foreground_color': template_data_list[0]['style']['foreground_color'],
                    'background_color': template_data_list[0]['style']['background_color'],
                    'common_items': npc['common_item']
                }
                
                return render(request, 'gesmjgenerator/display/npc_sheet.html', {
                    'npc': data,
                    'scene_content' : scene_data,
                    'location_content': location_data, 
                    'location_add_list' : location_full_list,
                    'location_count' : location_count,
                    'npc_id' : npc['id'],
                    'game_name':game_name,
                    'note': note[0] if note != '' else ''})
            else:
                return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
        else:
            return HttpResponseRedirect(reverse('gesmjgenerator:failed_fatal_error_view',args=[game_name]))
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})
        

@login_required
@ensure_csrf_cookie
@csrf_protect
def display_overview_view(request, game_name):
    """
    :param request:
    :param game_name
    :return: display game overview
    """
    if request.user.is_authenticated:
        game_id = Game.objects.only('id').get(name=game_name, owner_uuid_id=request.user.id).id
        note = Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).values_list('note', flat=True) if Note.objects.filter(owner_uuid_id=request.user.id, id_game_id_id=game_id).exists() else ''
        
        return render(request, 'gesmjgenerator/display/game_overview.html', {
            'game_name':game_name, 
            'game_id': game_id,
            'note': note[0] if note != '' else '',
        })
        
    else:
        loginForm = LoginForm()
        return render(request, 'gesmjgenerator/index.html', {'loginForm': loginForm})